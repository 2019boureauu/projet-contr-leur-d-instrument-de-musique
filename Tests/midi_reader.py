import midi


notes_dic={40:'E2',41:'F2',42:'F#2',43:'G2',43:'G#2',44:'A2',45:'A#2',46:'B2',47:'C3',48:'C#3',49:'D3',50:'D#3',51:'E3'}

class partition:
    """On traite la partition comme un objet. Il faut faire attention au fait que la caractéristique 'Notes' contient les temps en ticks, pour avoir
    accès aux notes, il faut faire partition.get_notes"""

    def __init__(self,fichier):
        pattern = midi.read_midifile(fichier)
        Notes=[]
        time=0
        for event in pattern[2]:
            if event.name == 'Note On':
                time+=event.tick
                Notes.append([time,0,notes_dic[event.data[0]]])
            if event.name == 'Note Off' and notes_dic[event.data[0]]==Notes[-1][2] :
                time+=event.tick
                Notes[-1][1]=time
            elif event.name == 'Note on' and Notes[-1][2] == 0:
                return 'Erreur : le fichier n\'est pas jouable sur une seule corde'
        self.resolution=pattern.resolution
        self.tempo=80
        self.notes=Notes


    def get_notes(self):
        ticks_to_ms=60*1000/(self.tempo*self.resolution)
        notes_en_ms=[]
        for i in range(0,len(self.notes)):
            notes_en_ms.append([0,0,0])
            notes_en_ms[i][0]=self.notes[i][0]*ticks_to_ms
            notes_en_ms[i][1]=self.notes[i][1]*ticks_to_ms
            notes_en_ms[i][2]=self.notes[i][2]
        return notes_en_ms

"partition=partition('demo1.mid')
"print(partition.get_notes())