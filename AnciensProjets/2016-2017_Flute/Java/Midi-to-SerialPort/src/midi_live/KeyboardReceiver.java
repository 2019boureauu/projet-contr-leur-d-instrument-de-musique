package midi_live;

import javax.sound.midi.MidiMessage;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;

/**
 * Receveur Midi.
 * Cette classe impl�mente la classe Receiver de Java et modifie la m�thode "send" 
 * afin de traiter les messages Midi aux lieux de les retransmettre vers un s�quenceur.
 */
public class KeyboardReceiver implements Receiver {

	private View view;
	private ManageMidi mm;
	
	/**
	 * Constructeur de la classe.
	 * 
	 * @param viewIn vue affichant les informations re�ues
	 * @param mmIn mod�le du dispositif midi � lier
	 */
	public KeyboardReceiver(View viewIn, ManageMidi mmIn) {
		this.view = viewIn;
		this.mm = mmIn;
	}
	
	
	/**
	 * M�thode permettant de fermer le receveur.
	 * (Actuellement cette m�thode n'execute aucune instruction)
	 */
	@Override
	public void close() {
		// TODO Auto-generated method stub
	}
	
	
	/**
	 * M�thode appel�e d�s que le receveur KeyboardListener re�oit un �v�nement midi.
	 * Redirige le message midi de l'�v�nement vers le mod�le ManageMidi (m�thode midiToNote).
	 * Retransmet �galement le message midi � la vue.
	 * 
	 * @param midiMsg message midi intercept�
	 * @param timeStamp �tiquette temporelle du message midi (inint�ressant si on ne souhaite pas enregistrer les messages re�us)
	 */
	@Override
	public void send(MidiMessage midiMsg, long timeStamp) {
		ShortMessage msg = (ShortMessage) midiMsg;
		// Message log
		String s = "Midi message. Commande : " + msg.getCommand() + ", Channel : " + msg.getChannel() + ", Note : " + msg.getData1() + ", Intensit� : " + msg.getData2();
		this.view.appendLog(s, true);
		// On envoit la note � la pyboard
		this.mm.midiToNote(msg);
	}

}
