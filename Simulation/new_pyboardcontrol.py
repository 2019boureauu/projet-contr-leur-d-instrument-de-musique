 # main.py -- put your code here!
import time
import asyncio


# Ce dictionnaire contient les pas correspondant áchaque note
note_step = {
"E2" : 88,
"F2" : 158,
"F#2" : 220,
"G2" : 274,
"G#2" : 320,
"A2" : 356,
"A#2" : 384,
"B2" : 420,
"C3" : 450,
"C#3" : 476,
"D3" : 500,
"D#3" : 520,
}

Tempo = 150 #tempo en BPM


note_de_ref='E2'

"""
Cette fonction lit des données ligne par ligne sur un flux d'entrée,
les interprète comme des nom de note anglosaxons, et écrit les notes
à l'italienne sur son flux de sortie.
L'idée est de simuler le comportement de la pyboard qui reçoit les noms de notes
et envoie des commandes aux actionneurs pour jouer la note sur la guitare.
"""

async def pyboard(status_writer, status_reader,reader_midi, bass_writer) :
  # On lit une ligne sur le flux d'entrée
  l = await reader_midi.readline()
  # Tant qu'on lit quelque chose
  while not ((l is None) or (len(l) == 0)) :
    data = l.decode('utf_8').strip()
    data = data.split("@")
    status_writer.write(('\r\n').encode())
    await status_writer.drain()
    print("MIDI data:", data)
    #basse est sous la forme pos_chariot@pos_distributeur@pos_servo
    b = await status_reader.readline()
    basse = b.decode('utf_8').strip().split("@")
    print("Bass status:", basse)
    char=int(basse[0])
    distr=basse[1]
    servo=basse[2]
    # On calcule le pas de la note reçue
    current_note = data[0]
    step=note_step[current_note]
    print("Target pos:", step)
    if(len(data)==2 and (data[0] in note_step.keys())):
      # Si on a bien reçu deux mots séparés par un @ et que le premier
      # est une note, on a bien en présence d'une requêtes du protocole
      if(data[1]=="START"):
        if char != step : # On n'est pas à la bonne position
          while distr == 'down' :   # tant que le doigt est appuyé sur la corde
            bass_writer.write("high@1\r\n".encode()) # on le relève
            await bass_writer.drain()
            await asyncio.sleep(1/100)
            status_writer.write('\r\n'.encode()) # On consulte de nouveau l'état de la basse
            await status_writer.drain()
            b = await status_reader.readline()
            basse = b.decode('utf_8').strip().split("@")
            print("Bass status:", basse)
            char=int(basse[0])
            distr=basse[1]
            servo=basse[2]
          # Le doigt est relevé, on bouge maintenant le chariot
          while char != step : # Tant qu'on n'est pas au bon endroit
            bass_writer.write((str(step)+"@0\r\n").encode())
            await bass_writer.drain()
            await asyncio.sleep(1/100)
            status_writer.write('\r\n'.encode()) # On consulte de nouveau l'état de la basse
            await status_writer.drain()
            b = await status_reader.readline()
            basse = b.decode('utf_8').strip().split("@")
            print("Bass status:", basse)
            char=int(basse[0])
            distr=basse[1]
            servo=basse[2]
          # On abaisse le doigt
          while distr == 'high' :   # tant que le doigt n'est pas appuyé sur la corde
            bass_writer.write(("down@1\r\n").encode())
            #@1 signifie que l'info concerne le distributeur
            await bass_writer.drain()
            await asyncio.sleep(1/100)
            status_writer.write('\r\n'.encode()) # On consulte de nouveau l'état de la basse
            await status_writer.drain()
            b = await status_reader.readline()
            basse = b.decode('utf_8').strip().split("@")
            print("Bass status:", basse)
            char=int(basse[0])
            distr=basse[1]
            servo=basse[2]
          # et on gratte la corde
          bass_writer.write(("gratte@2\r\n").encode())
          await bass_writer.drain()
          await asyncio.sleep(1/100)
      elif(data[1]=="STOP"):
        distr='high'
        bass_writer.write(("high@1\r\n").encode())
        #@1 signifie que l'info concerne le distributeur
        await bass_writer.drain()
        await asyncio.sleep(1/10000)
    l = await reader_midi.readline()           
  bass_writer.close()
  await bass_writer.wait_closed()

"""
Cette fonction traite les connexions au serveur qui traduit les notes MIDI
"""
async def handle_pyboard(status_writer, status_reader,reader_midi, bass_writer) :
  asyncio.create_task(pyboard(status_writer, status_reader,reader_midi, bass_writer))
