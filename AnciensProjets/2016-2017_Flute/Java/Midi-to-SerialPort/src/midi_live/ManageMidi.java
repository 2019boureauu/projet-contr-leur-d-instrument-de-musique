package midi_live;

import java.util.ArrayList;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Transmitter;

/**
 * Mod�le du dispositif Midi.
 * Regroupe toutes les m�thodes permettant de rechercher, de se connecter et de se d�connecter 
 * des differents dispositifs Midi reli�s � l'ordinateur.
 */
public class ManageMidi {
	
	private View view;
	private MidiCOMController mcController;
	
	/**
	 * Tableau contenant les dispositifs midi accessibles
	 */
	private ArrayList<MidiDevice> availableDevices = new ArrayList<MidiDevice>();
	
	private MidiDevice device;
	private Transmitter transmitter;
	private KeyboardReceiver receiver;
	
	/**
	 * Initialise le lien avec la vue.
	 * 
	 * @param viewIn vue � lier au mod�le
	 */
	public void initView(View viewIn) {
		this.view = viewIn;
	}
	
	/**
	 * Initialise le lien avec le controlleur.
	 * 
	 * @param mcControllerIn controlleur � lier au mod�le
	 */
	public void initController(MidiCOMController mcControllerIn) {
		this.mcController = mcControllerIn;
	}
	
	
	/**
	 * Liste les diff�rents dispositifs midi accessibles depuis l'ordinateur.
	 * 
	 * @return availableDevicesArray tableau contenant les noms des dispositifs midi accessibles
	 */
	public String[] listAvailableDevices() {
        MidiDevice midiDevice;
        MidiDevice.Info[] infos = MidiSystem.getMidiDeviceInfo();
        for (int i = 0; i < infos.length; i++) {
            try {
                midiDevice = MidiSystem.getMidiDevice(infos[i]);
                this.availableDevices.add(midiDevice);
                // Remarque : on ne teste pas si le dispositif Midi peut emettre un message Midi (Midi output)
                // Ceci est du au fait qu'il est impossible d'afficher les transmetteurs du dispositifs tant qu'il n'a pas �t� ouvert.
                // Au lieu d'ouvrir tous les dispositifs, on laisse � l'utilisateur le soin de choisir le bon dispositif Midi.
            } 
            catch (MidiUnavailableException e) {
            	e.printStackTrace();
            	this.view.setError("Une erreur est survenue lors de la recherche de dispositifs Midi. (" + e.toString() + ")");
            }
        }
        
        String[] availableDevicesArray = new String[this.availableDevices.size()];
        
        for(int i=0 ; i < this.availableDevices.size() ; i++) {
        	availableDevicesArray[i] = this.availableDevices.get(i).getDeviceInfo().getName();
        }
        
        return(availableDevicesArray);
    }
	
	
	/**
	 * Setter permetant de fixer le dispositif midi auquel on veut se connecter.
	 * 
	 * @param deviceIndex index du dispositif midi dans le tableau availableDevices
	 */
	public void setDevice(int deviceIndex) {
		try {
			this.device = this.availableDevices.get(deviceIndex);
			this.view.appendLog("Midi device : " + this.device.getDeviceInfo().getName(), false);
		}
		catch(ArrayIndexOutOfBoundsException e) {
			e.printStackTrace();
			this.view.setError("Aucun dispositif Midi avec le nom indiqu� trouv�. (" + e.toString() + ")");
		}
	}
	
	
	/**
	 * Permet de se connecter au dispositif midi pr�alablement s�lectionn� par la m�thode setDevice.
	 * Initialise �galement le receveur midi (classe KeyboardReceiver) et le transmetteur midi (classe Transmitter de Java).
	 */
	public void connect() {
		if(this.device == null) {
			this.view.setError("Connexion impossible car le dispositif Midi n'a pas �t� indiqu�.");
		}
		else {
			// On se connecte au dispositif Midi
			try {
				this.device.open();
			} catch (MidiUnavailableException e) {
				e.printStackTrace();
				this.view.setError("Erreur lors de la connexion au dispostif. (" + e.toString() + ")");
			}
			
			// On extrait le transmetteur du dispositif
			try {
				this.transmitter = this.device.getTransmitter();
			} catch (MidiUnavailableException e) {
				this.view.setError("Le dispositif Midi ne poss�de pas d'�metteur. (" + e.toString() + ")");
				e.printStackTrace();
			}
			
			// On demande au transmetteur d'�mettre sur notre �metteur. 
			this.receiver = new KeyboardReceiver(this.view, this); // Note: notre �metteur est une impl�mentation personnelle de la classe Receiver
			this.transmitter.setReceiver(this.receiver);
			
			this.view.appendLog("Dispositif Midi connect�.", false);
		}
	}
	
	/**
	 * Permet de d�connecter le dispositif midi ainsi que son receveur et son transmetteur.
	 */
	public void disconnect() {
		this.transmitter.close();
		this.receiver.close();
		this.device.close();
		
		this.view.appendLog("Dispositif Midi d�connect�.", false);
	}
	
	
	/**
	 * Transmet un message midi au controlleur (classe MidiCOMController) pour qu'il le traite (m�thode midiToNote).
	 * 
	 * @param msg message midi � transmettre
	 * 
	 * @see midi_live.MidiCOMController#midiToNote(ShortMessage)
	 */
	public void midiToNote(ShortMessage msg){
		this.mcController.midiToNote(msg);
	}
}
