package midi_live;

/**
 * Classe regroupant les initialisations des mod�les, du controlleur et de la vue.
 * Executez cette classe pour lancer le programme. 
 */
public class Main {
	
	/**
	 * Timeout lors de la connection au port COM. Valeur par d�fault : {@value #TIMEOUT} 
	 */
    final static int TIMEOUT = 2000;
    
    /**
     * Baudrate (nombre de bits par seconde) pour la connection au port COM. Valeur par d�fault : {@value #BAUDRATE} 
     */
    final static int BAUDRATE = 115200;
    
    /**
     * Tableau contenant les noms des notes index�s de 0 (->'C') � 11 (->'B'). Valeur par d�fault : {@value #NOTE_NAMES} 
     */
    final static String[] NOTE_NAMES = new String[]{"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"};

    // Valeurs ASCII utiles
    /**
     * Valeur ASCII du retour � la ligne. Valeur par d�fault : {@value #NEW_LINE_ASCII}
     */
    final static int NEW_LINE_ASCII = 10;
    
    /**
     * Valeur ASCII de la touche "Enter". Valeur par d�fault : {@value #ENTER_ASCII} 
     */
    final static int ENTER_ASCII = 13;

    
	public static void main(String[] args) {
		ManagePorts mp = new ManagePorts();
		ManageMidi mm = new ManageMidi();
		
		MidiCOMController mcController = new MidiCOMController(mp, mm);
		mm.initController(mcController);
		
		View view = new View(mp, mm);
		mp.initView(view);
		mm.initView(view);
	}

}
