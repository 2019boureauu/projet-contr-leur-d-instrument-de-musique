Avant de lancer les programmes assurez-vous d'avoir installé les modules suivants :

- asyncio
- tkinter
- time
- midi

Pour ce dernier module (midi) vous ne le trouverez sans doute pas si vous faites simplement 'pip install midi'.
Vous pouvez l'installer en tapant la commande suivante dans le terminal : pip install git+https://github.com/vishnubob/python-midi@feature/python3

#SIMULATION DU CONTROLEUR :


La simulation se lance en exécutant le programme 'new_systemecomplet.py' dans le dossier Simulation
Le programme 'new_read_midi.py' est celui qui serait présent sur l'ordinateur, il envoie des commandes de notes à 'new_pyboardcontrol.py'.
Le programme 'new_pyboardcontrol.py' est celui qui serait présent sur la carte, il envoie des commandes d'actions à 'new_bassguitare.py'.
Le programme new_bassguitare.py affiche les actions commandées par la carte, il envoie des infos à 'new_pyboardcontrol.py'.

#LECTEUR MIDI:

Le lecteur midi est dans le fichier midi_reader.py. Un fichier midi demo1.mid est disponible pour faire des tests,
vous pouvez importer votre propre fichier midi pour tester.

#CLAVIER VIRTUEL:


