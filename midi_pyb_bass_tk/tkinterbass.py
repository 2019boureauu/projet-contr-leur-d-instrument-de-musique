import asyncio
import tkinter as tk

####
# On utilise asyncio pour avoir plusieurs tâches qui s'exécutent en même temps, 
# mais de manière coopérative. Aini, une tâche ne cède du temps aux autres que 
# volontairement, quand elle attend autre chose, grâce à "await"
# Ces tâches sont déclarées "async" de façon à pouvoir être gérées par un ordonnanceur 
# de tâche qui est mis en œuvre par asyncio.run().
#
# La communication entre les tâches se fait en mode client-serveur. On crée un
# serveur pour chaque tâche, et ceux qui veulent utiliser le résultat de la tâche
# se connectent au serveur.
###

# Abscisse des cases
cases = {1:85, 3:220, 4:275, 5:320}
# Cases correpondant aux notes
notes = {"Fa":1, "Sol":3, "La":5}

# Abscisse du chariot, au départ, il est  sur la case 1
x = cases[1]
# Ordonnées du chariot (fixe)
y=300

# Couleur et rayon du disque qui représente le chariot
color="black"
rayon = 10

# Déplacement élémentaire du chariot, en pixels
dx = 5

# Le canvas dans lequel est dessiné le manche
bass_canvas = None
# L'ovale qui représente le chariot
carriage = None

# Intervalle de mise à jour du canvas en secondes
updateInterval = 1/30  # Update canvas 30 times per second

"""
Cette fonction est chargée de mettre à jour le canvas pour que les déplacements 
du chariot soient visibles. Elle appelle simplement la méthode update du canvas pour 
qu'il se redessine, puis s'endort jusqu'au prochain rafraîchissement.
"""
async def updateCanvas() :
  while True:
    bass_canvas.update()
    await asyncio.sleep(updateInterval)

"""
Cette fonction crée une nouvelle fenêtre principale pour y afficher le manche de la basse.
tkmain est la fenêtre racine de tkinter, evt_loop est la boucle d'ordonnancement d'asyncio
"""
def createBassWindow(tkmain, evt_loop) :
  global carriage, bass_canvas
  # on crée la fenêtre principale
  bass_win = tk.Toplevel(tkmain)
  bass_win.title("Guitare basse")
  bass_win.geometry("700x600")
  # et le canvas pour y dessiner le manche de la basse
  bass_canvas = tk.Canvas(bass_win, width=600, height=500, bg='ivory')
  bass_canvas.pack()
  bass_canvas.place(x=50,y=50)
  # On dessine les frettes
  bass_canvas.create_line(50, 250, 530, 250, width=2, fill="black")
  bass_canvas.create_line(50, 350, 530, 350, width=2, fill="black")
  bass_canvas.create_line(50, 250, 50, 350, width=2, fill="black")
  bass_canvas.create_line(530, 250, 530, 350, width=2, fill="black")
  bass_canvas.create_line(125, 250, 125, 350, width=2, fill="black")
  bass_canvas.create_line(190, 250, 190, 350, width=2, fill="black")
  bass_canvas.create_line(250, 250, 250, 350, width=2, fill="black")
  bass_canvas.create_line(300, 250, 300, 350, width=2, fill="black")
  bass_canvas.create_line(340, 250, 340, 350, width=2, fill="black")
  bass_canvas.create_line(370, 250, 370, 350, width=2, fill="black")
  bass_canvas.create_line(405, 250, 405, 350, width=2, fill="black")
  bass_canvas.create_line(435, 250, 435, 350, width=2, fill="black")
  bass_canvas.create_line(463, 250, 463, 350, width=2, fill="black")
  bass_canvas.create_line(488, 250, 488, 350, width=2, fill="black")
  bass_canvas.create_line(510, 250, 510, 350, width=2, fill="black")
  # On dessine le chariot
  carriage = bass_canvas.create_oval(x-rayon,y-rayon,x+rayon,y+rayon, fill=color)
  # On ajoute la tâche de rafraîchissement du canvas à la boucle d'événements d'asyncio
  evt_loop.create_task(updateCanvas())

"""
Déplace le chariot à la position dest
"""
def moveCarriageTo(dest) :
  if dest == x :
    return
  if x < dest :
    elementaryMove(dest, dx)
  else :
    elementaryMove(dest, -dx)

"""
Effectue un déplacement élémentaire du chariot 
jusqu'à ce que la position voulue soit atteinte.
"""
def elementaryMove(dest, dx) :
  global x
  x += dx
  bass_canvas.coords(carriage, x-rayon, y-rayon, x+rayon, y+rayon)
  if (dx > 0 and x < dest) or (dx < 0 and x > dest) :
    bass_canvas.after(10, elementaryMove, dest, dx)


"""
Cette fonction lit des données ligne par ligne sur un flux d'entrée,
les interprète comme des noms de note, et déplace le chariot sur le manche 
à la position correspondante.
L'idée est de simuler le comportement du système de contrôle de la guitare qui
reçoit des commandes et déplace le chariot sur une case du manche.
"""
async def bassguitar(reader, writer) :
  while True :
    # On lit une ligne sur le flux d'entrée
    l = await reader.readline()
    # Si on atteint la fin du flux, on arrête
    if (l is None) or (len(l) == 0) :
      break
    # On décode les données brutes (bytes) pour obtenir une chaîne de caractères
    # et on supprime les blancs en tête et en fin de chaîne
    l = l.decode().strip()
    print("#BASS processing " + l)
    # On récupère la case associée à la note.
    if not (l in notes) :
      print("#Error: unknown note: " + l)
      continue
    c = notes[l]
    # On récupère la position de la case sur le manche
    if not (c in cases) :  # Ne devrait jamais se produire
      print("#Error: unknown case: " + c)
      continue
    p = cases[c]
    # On déplace le chariot à la bonne position
    print("Moving to slot " + str(c) + " at position " + str(p))
    moveCarriageTo(p)

  # Quand c'est fini, on ferme le flux de sortie pour indiquer au suivant qu'il n'y a plus rien à lire.
  writer.close()
  await writer.wait_closed()

"""
Cette fonction traite les connexions au serveur qui traduit les notes en positions
"""
async def handle_guitar(bassreader, basswriter) :
	asyncio.create_task(bassguitar(bassreader, basswriter))

if __name__ == "__main__":
  """
  Ce programme de test crée un serveur sur la machine (127.0.0.1 est l'adresse de la 
  machine locale) qui écoute les requêtes de connexion sur le port 10000.
  Quand une connexion est demandée, la fonction handle_guitar est appelée.

  Le programme se connecte ensuite à ce serveur, et lui envoie une suite de commandes
  (voir le scenario dans la variable "input".
  """
  async def main() :
    # Creation de l'interface graphique
    tk_main = tk.Tk()
    tk_main.withdraw()  # cacher la fenêtre racine
    createBassWindow(tk_main, asyncio.get_event_loop())
    
    # Lancement du serveur sur le port 10000
    await asyncio.start_server(handle_guitar, host="127.0.0.1", port=10000)
    # Connexion au serveur, ce qui rend une paire de flux permettant 
    # de recevoir et d'envoyer des données au serveur
    (bassreader, basswriter) = await asyncio.open_connection(host="127.0.0.1", port=10000)
    # Données utilisées pour tester le programme = liste de paires (note, duree en ms)
    input = (("La",3000), ("Fa",2000), ("Sol",1500))
    # Pour chaque donnée à tester
    for (n, d) in input :
      # On envoie la donnée au serveur (avec fin de ligne pour que readline marche bien)
      basswriter.write((n + "\r\n").encode())
      print("Playing " + n)
      # On attend que les données soient parties
      await basswriter.drain()
      # On attend la durée
      await asyncio.sleep(d/1000)

  # On exécute le programme de test comme une tâche asyncio
  asyncio.run(main())
