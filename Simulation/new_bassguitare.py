import asyncio
import tkinter as tk

# Abscisse du chariot, au départ, il est  sur la case 1
x = 100
# Ordonnées du chariot (fixe)
y=300

# Couleur et rayon du disque qui représente le chariot
color="black"
rayon = 10

# Déplacement élémentaire du chariot, en pixels
dx = 2

# Le canvas dans lequel est dessiné le manche
bass_canvas = None
moteur = None

# L'ovale qui représente le chariot
carriage = None
dist = 'high'
mot = 'left'

# Le chariot est-il en mouvement ?
moving=False

# Intervalle de mise à jour du canvas en secondes
updateInterval = 1/100  # Update canvas 1000 times per second

"""
Cette fonction est chargée de mettre à jour le canvas pour que les déplacements 
du chariot soient visibles. Elle appelle simplement la méthode update du canvas pour 
qu'il se redessine, puis s'endort jusqu'au prochain rafraîchissement.
"""
async def updateCanvas() :
  while True:
    bass_canvas.update()
    await asyncio.sleep(updateInterval)

"""
Cette fonction crée une nouvelle fenêtre principale pour y afficher le manche de la basse.
tkmain est la fenêtre racine de tkinter, evt_loop est la boucle d'ordonnancement d'asyncio
"""
def createBassWindow(tkmain, evt_loop) :
  global carriage, bass_canvas, moteur
  # on crée la fenêtre principale
  bass_win = tk.Toplevel(tkmain)
  bass_win.title("Guitare basse")
  bass_win.geometry("700x600")
  # et le canvas pour y dessiner le manche de la basse
  bass_canvas = tk.Canvas(bass_win, width=600, height=500, bg='ivory')
  bass_canvas.pack()
  bass_canvas.place(x=50,y=50)
  # On dessine les frettes
  bass_canvas.create_line(50, 250, 530, 250, width=2, fill="black")
  bass_canvas.create_line(50, 350, 530, 350, width=2, fill="black")
  bass_canvas.create_line(50, 250, 50, 350, width=2, fill="black")
  bass_canvas.create_line(530, 250, 530, 350, width=2, fill="black")
  bass_canvas.create_line(125, 250, 125, 350, width=2, fill="black")
  bass_canvas.create_line(190, 250, 190, 350, width=2, fill="black")
  bass_canvas.create_line(250, 250, 250, 350, width=2, fill="black")
  bass_canvas.create_line(300, 250, 300, 350, width=2, fill="black")
  bass_canvas.create_line(340, 250, 340, 350, width=2, fill="black")
  bass_canvas.create_line(370, 250, 370, 350, width=2, fill="black")
  bass_canvas.create_line(405, 250, 405, 350, width=2, fill="black")
  bass_canvas.create_line(435, 250, 435, 350, width=2, fill="black")
  bass_canvas.create_line(463, 250, 463, 350, width=2, fill="black")
  bass_canvas.create_line(488, 250, 488, 350, width=2, fill="black")
  bass_canvas.create_line(510, 250, 510, 350, width=2, fill="black")
  # On dessine le chariot
  carriage = bass_canvas.create_oval(x-rayon,y-rayon,x+rayon,y+rayon, fill='black')
  moteur = bass_canvas.create_oval(530,y-rayon,550,y+rayon, fill='blue')
  # On ajoute la tâche de rafraîchissement du canvas à la boucle d'événements d'asyncio
  evt_loop.create_task(updateCanvas())

"""
Déplace le chariot à la position dest
"""
def moveCarriageTo(dest) :
  global x, moving
  if dest == x :
    return
  moving = True
  if x < dest :
    elementaryMove(dest, dx)
  else :
    elementaryMove(dest, -dx)

"""
Effectue un déplacement élémentaire du chariot 
jusqu'à ce que la position voulue soit atteinte.
"""
def elementaryMove(dest, dx) :
  global x, moving
  print("x:", x)
  x += dx
  bass_canvas.coords(carriage, x-rayon, y-rayon, x+rayon, y+rayon)
  if (dx > 0 and x < dest) or (dx < 0 and x > dest) :
    bass_canvas.after(3, elementaryMove, dest, dx)
  else :
    moving = False

def red():
  bass_canvas.itemconfigure(carriage,fill='red',width=0)

def black():
  bass_canvas.itemconfigure(carriage,fill='black',width=0)

def green():
  bass_canvas.itemconfigure(moteur,fill='green',width=0)

def blue():
  bass_canvas.itemconfigure(moteur,fill='blue',width=0)

def push(instruction):
  global dist
  print('dist:', dist)
  if instruction == 'high':
    bass_canvas.after(0,black)
    dist = 'high'
  elif instruction == 'down':
    bass_canvas.after(20,red)
    dist = 'down'

def gratte():
  global mot
  print('mot', mot)
  if mot == 'left':
    bass_canvas.after(0,green)
    mot = 'right'
  elif mot == 'right':
    bass_canvas.after(0,blue)
    mot = 'left'
  

async def bassguitar(reader, writer) :
  # On lit une ligne sur le flux d'entrée
  l = await reader.readline()
  # Tant qu'on lit quelque chose
#   writer.write(('\r\n').encode())
#   await writer.drain()
  while not ((l is None) or (len(l) == 0)) :
    # On décode les données brutes (bytes) pour obtenir une chaîne de caractères
    # et on supprime les blancs en tête et en fin de chaîne
    data = l.decode('utf_8').strip()
    data = data.split("@")
    print("Bass receives:", data)
    if data[1]=='0':
      print('déplacement en', data[0])
      moveCarriageTo(int(data[0]))
    elif data[1] == '1':
      print('push', data[0])
      push(data[0])
    elif data[1] == '2':
      print('gratte')
      gratte()
    l = await reader.readline()
  # Quand c'est fini, on ferme le flux de sortie pour indiquer au suivant qu'il n'y a plus rien à lire.
  writer.close()
  await writer.wait_closed()

async def bassguitar_status(reader, writer) :
  global x, moving
  # On lit une ligne sur le flux d'entrée
  l = await reader.readline()
  # Tant qu'on lit quelque chose
  while not ((l is None) or (len(l) == 0)) :
    # Attendre la fin d'un mouvement avant de répondre
    while moving :
      await asyncio.sleep(1/100)
    writer.write((str(x)+"@"+dist+"@"+mot+'\r\n').encode())
    await writer.drain()
    await asyncio.sleep(1/100)
    l = await reader.readline()
  # Quand c'est fini, on ferme le flux de sortie pour indiquer au suivant qu'il n'y a plus rien à lire.
  writer.close()
  await writer.wait_closed()

"""
Cette fonction traite les connexions au serveur qui traduit les notes en positions
"""
async def handle_guitar(bassreader, basswriter) :
	asyncio.create_task(bassguitar(bassreader, basswriter))

async def handle_guitar_status(status_reader, status_writer) :
	asyncio.create_task(bassguitar_status(status_reader, status_writer))
