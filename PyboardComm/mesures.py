#!/usr/bin/env python3
from pyboard import Pyboard # Une classe pour communiquer avec la pyboard

# Pour afficher les données
import matplotlib.pyplot as plt

# Pour enregistrer les données au format CSV
import csv

# Pour trouver la pyboard sous Unix et MacOS
import os
import fnmatch

# Pour faire des mesures régulièrement
import time

# Fonction rechaerchant une pyboard sous Unix ou Mac OS
def find_pyboard() :
	# look for pyboard v1 under Linux
	ttymodems = fnmatch.filter(os.listdir('/dev'), 'ttyACM*')
	if len(ttymodems) == 0:
		# look for pyboard v1 under MacOS
		ttymodems = fnmatch.filter(os.listdir('/dev'), 'tty.usbmodem*')
	if len(ttymodems) == 0:
		# look for wipy under MacOS
		ttymodems = fnmatch.filter(os.listdir('/dev'), 'tty.usbserial-*')
	if len(ttymodems) == 0:
		return None
	return '/dev/'+ttymodems[0]

# Remplacer par
# board = "COM8:"
# ou quelque chose de similaire sous windows si la pyboard est sur le port COM8
board = find_pyboard()
#
if board is None :
	print('Error: no pyboard found.')
	sys.exit(1)
	
# On crée l'objet pour communiquer avec la pyboard
pyb = Pyboard(board)
# On met la pyboard en mode "interprèteur de commande nu", sans prompt ni écho
pyb.enter_raw_repl()
# On allume la LED rouge pour indiquer que le programme démarre
pyb.exec("pyb.LED(1).on()")
# Création de la patte X19 en mode analogique->numérique
pyb.exec("pin=pyb.ADC(pyb.Pin('X19'))")

# Nombre de millisecondes au début de la série de mesures
start = int(pyb.eval("pyb.millis()"))

# On prend 10 mesures (ici, un LM36 est relié à la patte X19, donc on mesure la température
mesures = []
for _ in range(10) :
	# On évalue le temps écoulé depuis le début, en millisecondes, et la valeur de la patte X19
	(timestamp, value) = (pyb.eval("pyb.elapsed_millis("+str(start)+")"), pyb.eval("pin.read()"))
	# On convertit la data en int et la valeur en température selon les paramètre du LM36
	mesures.append((int(timestamp), (25+10*(3.3*int(value)/4095 - 0.750))))
	# On affiche un point pour montrer qu'il se passe quelque chose
	print(".", end = '', flush = True)
	# On attend une seconde avant la prochaine mesure
	time.sleep(1)

# On passe à la ligne
print()
# On affiche le tableau des mesures
print(mesures)
# On éteint la LED pour indiquer que c'est fini
pyb.exec("pyb.LED(1).off()")
# On sort du mode "interpèteur nu", ce qui permet de se connecter au REPL classique de la pyboard
pyb.exit_raw_repl()

with open("data.csv", "w") as f :
	csv_writer = csv.writer(f, dialect='excel')
	for (date, value) in mesures :
		csv_writer.writerow([date, value])

# On utilise matplotlib sur l'ordinateur pour afficher les mesures.
plt.plot([m[0] for m in mesures], [m[1] for m in mesures])
plt.show()
