package midi_live;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;

/**
 * Classe g�rant la vue.
 * Contient tous les �l�ments relatifs � l'interface graphique du programme.
 */
public class View {
	
	private ManagePorts mp;
	private ManageMidi mm;
	
	private JFrame f;
	private JComboBox<String> selectPort;
	private JComboBox<String> selectMidi;
	private JLabel error;
	private JTextArea log;
	private JTextField outputField;
	
	/**
	 * Constructeur de la classe.
	 * G�n�re la fen�tre de la vue.
	 * 
	 * @param mpIn mod�le du port s�rie li� � la vue
	 * @param mmIn mod�le du dispositif midi li� � la vue
	 */
	public View(ManagePorts mpIn, ManageMidi mmIn) {
		// Initialisation des mod�les et controlleurs
		this.mp = mpIn;
		this.mm = mmIn;
		
		// Initialisation Fen�tre
		this.f = new JFrame("Midi to serial COM port");
		this.f.setVisible(true);
		this.f.setSize(1200, 600);
		this.f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel p = new JPanel();
		p.setLayout(new GridBagLayout());
		GridBagConstraints c;
		this.f.add(p);
		
		// Constantes de dimentionnement
		double weightX = 0.4;
		double weightY = 0.5;
		
		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = weightX;
		c.weighty = weightY;
		c.gridwidth = 4;
		p.add(new JLabel("Configuration port COM et Midi device"), c);
		
		// Selection port COM
		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 1;
		c.weightx = 0.1;
		c.weighty = weightY;
		c.insets = new Insets(0,5,0,0);
		c.anchor = GridBagConstraints.LINE_START;
		p.add(new JLabel("<html>S�lectionez le port<br>COM d�sir� :</html>"), c);
		
		c = new GridBagConstraints();
		c.gridx = 1;
		c.gridy = 1;
		c.weightx = weightX;
		c.weighty = weightY;
		c.gridwidth = 3;
		c.fill = GridBagConstraints.HORIZONTAL;
		this.selectPort = new JComboBox<String>();
		p.add(this.selectPort, c);
		
		
		// Selection Midi device
		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 2;
		c.weightx = 0.1;
		c.weighty = weightY;
		c.insets = new Insets(0,5,0,0);
		c.anchor = GridBagConstraints.LINE_START;
		p.add(new JLabel("<html>S�lectionez l'instrument<br>Midi d�sir� :</html>"), c);

		c = new GridBagConstraints();
		c.gridx = 1;
		c.gridy = 2;
		c.weightx = weightX;
		c.weighty = weightY;
		c.gridwidth = 3;
		c.fill = GridBagConstraints.HORIZONTAL;
		this.selectMidi = new JComboBox<String>();
		p.add(this.selectMidi, c);
		
		
		// Boutons Set, Refresh, Connect et Disconnect
		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 3;
		c.weightx = weightX;
		c.weighty = weightY;
		c.gridwidth = 2;
		JButton submitRefresh = new JButton("Refresh");
		submitRefresh.addActionListener(new ButtonListener(this));
		p.add(submitRefresh, c);
		
		c = new GridBagConstraints();
		c.gridx = 2;
		c.gridy = 3;
		c.weightx = weightX;
		c.weighty = weightY;
		c.gridwidth = 2;
		JButton submitSet = new JButton("Set");
		submitSet.addActionListener(new ButtonListener(this));
		p.add(submitSet, c);
		
		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 4;
		c.weightx = weightX;
		c.weighty = weightY;
		c.gridwidth = 2;
		JButton submitDisconnect = new JButton("Disconnect");
		submitDisconnect.addActionListener(new ButtonListener(this));
		p.add(submitDisconnect, c);
		
		c = new GridBagConstraints();
		c.gridx = 2;
		c.gridy = 4;
		c.weightx = weightX;
		c.weighty = weightY;
		c.gridwidth = 2;
		JButton submitConnect = new JButton("Connect");
		submitConnect.addActionListener(new ButtonListener(this));
		p.add(submitConnect, c);
		
		
		// Espace reserv� aux messages d'erreur
		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 5;
		c.weightx = weightX;
		c.weighty = 0.7;
		c.gridwidth = 4;
		c.gridheight = 2;
		this.error = new JLabel("<html><div style=\"width:300px\"></div></html>");
		p.add(this.error, c);
		
		
		// Log
		c = new GridBagConstraints();
		c.gridx = 4;
		c.gridy = 0;
		c.weightx = 0.8;
		c.weighty = weightY;
		c.gridwidth = 8;
		c.gridheight = 5;
		c.fill = GridBagConstraints.BOTH;
		c.insets = new Insets(5,10,0,5);
		this.log = new JTextArea();
		Border border = BorderFactory.createLineBorder(Color.BLACK);
		this.log.setBorder(border);
		this.log.setLineWrap(true);
		this.log.setWrapStyleWord(true);
		this.log.setText(" >>> Fenetre de Log");
		p.add(this.log, c);
		
		c.anchor = GridBagConstraints.LINE_END;
		JScrollPane scroll = new JScrollPane(this.log);
		p.add(scroll,c);
		
		
		// Envoi de donn�es manuellement
		c = new GridBagConstraints();
		c.gridx = 4;
		c.gridy = 5;
		c.weightx = 0.8;
		c.weighty = weightY;
		c.gridwidth = 7;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0,10,0,0);
		this.outputField = new JTextField();
		border = BorderFactory.createLineBorder(Color.BLACK);
		this.outputField.setBorder(border);
		p.add(this.outputField, c);
		
		c = new GridBagConstraints();
		c.gridx = 11;
		c.gridy = 5;
		c.weightx = 0.1;
		c.weighty = weightY;
		JButton submitSend = new JButton("Send");
		submitSend.addActionListener(new ButtonListener(this));
		p.add(submitSend, c);
	}
	
	/**
	 * Demande aux mod�les ManageMidi et ManagePorts de chercher respectivement les dispositifs midi et les ports s�rie disponibles.
	 * 
	 * @see midi_live.ManageMidi#listAvailableDevices()
	 * @see midi_live.ManagePorts#listAvailablePorts()
	 */
	public void refreshCOMMidi() {
		this.selectPort.setModel(new DefaultComboBoxModel<String>(this.mp.listAvailablePorts()));
		this.selectMidi.setModel(new DefaultComboBoxModel<String>(this.mm.listAvailableDevices()));
		this.appendLog("Liste des ports COM et des dispositifs Midi rafraichie.", false);
	}
	
	/**
	 * Demande aux mod�les ManageMidi et ManagePorts de fixer respectivement le dispositif midi et le port s�rie s�lectionn�s.
	 * 
	 * @see midi_live.ManageMidi#setDevice(int)
	 * @see midi_live.ManagePorts#setCommPort(String)
	 */
	public void setCOMMidi() {
		String portName = (String) this.selectPort.getSelectedItem();
		int midiIndex = this.selectMidi.getSelectedIndex();
		
		this.mp.setCommPort(portName);
		this.mm.setDevice(midiIndex);
		
		this.appendLog("Port COM et dispositif Midi s�lectionn�s.", false);
	}
	
	/**
	 * Demande aux mod�les ManageMidi et ManagePorts de se connecter au dispositif midi et au port s�rie fix�s.
	 * 
	 * @see midi_live.ManageMidi#connect()
	 * @see midi_live.ManagePorts#setInputOutput()
	 * @see midi_live.ManagePorts#initListener()
	 */
	public void connect() {
		// Connexion du port serial
		this.mp.setInputOutput();
		this.mp.initListener();
		// Connexion du dispositif midi
		this.mm.connect();
		
		this.appendLog("Port COM et dispositif Midi en marche.", false);
	}
	
	/**
	 * Demande aux mod�les ManageMidi et ManagePorts de d�connecter le dispositif midi et le port s�rie.
	 * 
	 * @see midi_live.ManageMidi#disconnect()
	 * @see midi_live.ManagePorts#disconnect()
	 */
	public void disconnect() {
		this.mp.disconnect();
		this.mm.disconnect();
		
		this.appendLog("Port COM et dispositif Midi arr�t�s.", false);
	}
	
	/**
	 * Envoyer une instruction (inscrite manuellement dans l'interface graphique) au dispositif du port s�rie.
	 */
	public void sendCOMData() {
		String s = this.outputField.getText();
		this.mp.writeData(s);
		this.appendLog("[COM_In] : " + s, true);
	}
	

	/**
	 * Inscrit un message dans la console de log.
	 * Un param�tre supl�mentaire permet de distinguer les messages d'input 
	 * (vers le port s�rie) et d'output (re�u du port s�rie ou du dispositif midi).
	 * 
	 * @param logTxt message � inscrire
	 * @param isInput indique s'il s'agit d'un message d'input ou non
	 */
	public void appendLog(String logTxt, boolean isInput) {
		if(isInput) {
			this.log.append("\n > " + logTxt);
		}
		else {
			this.log.append("\n >>> " + logTxt);
		}
	}
	
	/**
	 * Affiche l'erreur entr�e en param�tre dans le cadre pr�vu � cet effet.
	 * 
	 * @param errorIn message d'erreur � afficher
	 */
	public void setError(String errorIn) {
		this.error.setText("<html><div style=\"width:200px\"><font color=red>" + errorIn + "</font></div></html>");
		this.appendLog("[Error] : " + errorIn, false);
	}	
}