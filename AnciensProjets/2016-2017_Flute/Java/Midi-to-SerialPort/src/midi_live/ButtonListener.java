package midi_live;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Classe g�rant les �v�nements qui surviennent lorsque l'on appuie sur un des boutons de l'interface graphique.
 * Cette classe impl�mente la classe ActionListener.
 */
public class ButtonListener implements ActionListener {
	
	private View view;
	
	/**
	 * Constructeur de la classe.
	 * 
	 * @param viewIn instance de la classe View qui �met les �v�nements
	 */
	public ButtonListener(View viewIn) {
		this.view = viewIn;
	}

	
	/**
	 * M�thode appel�e d�s que la classe ButtonListener re�oit un �v�nement.
	 * Elle d�termine l'origine de l'�v�nement et r�agit en cons�quence.
	 * 
	 * @param e Ev�nement capt� lorsque l'utilisateur clique sur un bouton
	 */
	public void actionPerformed(ActionEvent e) {
		
		// Rafraichissement des listes de ports COM et Midi Devices
		if(e.getActionCommand().equals("Refresh")) {
			this.view.refreshCOMMidi();
		}
		
		// Selection du port COM et du Midi device
		if(e.getActionCommand().equals("Set")) {
			this.view.setCOMMidi();
		}
		
		// Connexion du port COM et du Midi device
		if(e.getActionCommand().equals("Connect")) {
			this.view.connect();
		}
		
		// D�connexion du port COM et du Midi device
		if(e.getActionCommand().equals("Disconnect")) {
			this.view.disconnect();
		}
		
		// Envoi de donn�es manuellement
		if(e.getActionCommand().equals("Send")) {
			this.view.sendCOMData();
		}
	}
}
