package midi_live;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.TooManyListenersException;

import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;

/**
 * Mod�le du port s�rie.
 * Regroupe toutes les m�thodes permettant de rechercher, de se connecter et de se d�connecter 
 * des differents ports s�rie reli�s � l'ordinateur.
 * Cette classe repose majoritairement sur la librairie RXTX qui n'est pas fournie par d�faut dans Java.
 * Une documentation de cette librairie peut �tre trouv�e � l'adresse suivante : http://users.frii.com/~jarvi/rxtx/doc/index.html.
 */
public class ManagePorts {
	
	private CommPortIdentifier port;
	private SerialPort serialPort;
	private InputStream input;
	private OutputStream output;
	private View view;
	
	/**
	 * Initialise le lien avec la vue.
	 * 
	 * @param viewIn vue � lier au mod�le
	 */
	public void initView(View viewIn) {
		this.view = viewIn;
	}
	
	/**
	 * Liste les diff�rents ports s�rie accessibles depuis l'ordinateur.
	 * 
	 * @return availablePortsArray tableau contenant les noms des ports s�rie accessibles
	 */
	public String[] listAvailablePorts() {  
        Enumeration<CommPortIdentifier> ports = CommPortIdentifier.getPortIdentifiers();
        
        ArrayList<CommPortIdentifier> availablePorts = Collections.list(ports);
        String[] availablePortsArray = new String[availablePorts.size()];
        
        for(int i=0 ; i < availablePorts.size() ; i++) {
        	availablePortsArray[i] = availablePorts.get(i).getName();
        }
        
        return(availablePortsArray);
    }  
	
	
	/**
	 * Setter permetant de fixer le port s�rie auquel on veut se connecter.
	 * 
	 * @param portName nom du port s�rie � fixer
	 */
	public void setCommPort(String portName) {
		try {
			this.port = CommPortIdentifier.getPortIdentifier(portName);
			this.view.appendLog("Port COM : " + portName, false);
		} catch (NoSuchPortException e) {
			this.view.setError("Impossibilit� de se connecter au port s�rie s�lectionn�. (" + e.toString() + ")");
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Fixe et ouvre les canneaux d'input et d'output avec le dispositif du port s�rie.
	 * Le port s�rie doit �tre pr�alablement fix� avec la m�thode setCommPort.
	 */
	public void setInputOutput() {
		// On teste si le port n'est pas utilis� par un autre programme
        if (this.port.isCurrentlyOwned()) {
			this.view.setError("Le port s�rial est utilis� par un autre programme.");
        } 
        else {        	
            // Indique qui poss�de le port ainsi que le timeout
			try {
				this.serialPort = (SerialPort) this.port.open("Java_MidiToSerial", Main.TIMEOUT);
			} catch (PortInUseException e) {
				e.printStackTrace();
				this.view.setError("Impossible d'ouvrir le port s�rie. (" + e.toString() + ")");
			}
            
            // On pose les param�tres de la connection
            try {
				this.serialPort.setSerialPortParams(Main.BAUDRATE, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
			} catch (UnsupportedCommOperationException e) {
				e.printStackTrace();
				this.view.setError("Une erreur est survenue lors de la configuration du port s�rie. (" + e.toString() + ")");
			}  
   
            // Initialisation des canneaux d'entr�e et sortie
            try {
				this.input = this.serialPort.getInputStream();
				this.output = this.serialPort.getOutputStream(); 
			} catch (IOException e) {
				e.printStackTrace();
				this.view.setError("Erreur lors de l'obtention des cannaux d'entr�e et de sortie. (" + e.toString() + ")");
			}
            
            this.view.appendLog("Entr�e et sortie du port s�rial �tablies.", false);
        }
    }
	
	
	/**
	 * Initialise un listener qui va capter si un message est transmis par le dispositif du port s�rie.
	 * Ce listener est impl�ment� par la classe SerialPortListener.
	 * 
	 * @see midi_live.SerialPortListener
	 */
	public void initListener()	{
        try
        {
            this.serialPort.addEventListener(new SerialPortListener(this, this.view));
            this.serialPort.notifyOnDataAvailable(true);
        }
        catch (TooManyListenersException e)
        {
            e.printStackTrace();
            this.view.setError("Erreur lors de l'initiation du listener du port s�rie. (" + e.toString() + ")");
        }
    }
	
	
	/**
	 * Envoit une instruction sous forme de chaine de caract�res au dispositif du port s�rie.
	 * 
	 * @param data instruction � envoyer au dispositif
	 */
	public void writeData(String data) {
		byte[] dataBytes = data.getBytes();
		int l = dataBytes.length;
        try {
        	for(int i=0 ; i<l ; i++) {
        		this.output.write(dataBytes[i]);
        	}
        	this.output.write(Main.ENTER_ASCII);
            this.output.flush();
        }
        catch (Exception e) {
        	e.printStackTrace();
        	this.view.setError("Erreur lors de l'�criture des donn�es. (" + e.toString() + ")");
        }
    }
	
	
	/**
	 * Ferme les canneaux d'input et d'output et d�connecte le port s�rie.
	 */
	public void disconnect() {
        try {
            this.serialPort.removeEventListener();
            this.serialPort.close();
            this.input.close();
            this.output.close();
            this.view.appendLog("Port COM d�connect�.", false);
        }
        catch (Exception e) {
            e.printStackTrace();
            this.view.setError("Erreur lors de la d�connexion du port s�rie. (" + e.toString() + ")");
        }
    }
	
	/**
	 * Getter retournant le cannal d'input du port s�rie.
	 * 
	 * @return input cannal d'input du port s�rie.
	 */
	public InputStream getInput() {
		return(this.input);
	}
}