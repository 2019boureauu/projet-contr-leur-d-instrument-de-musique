package midi_file;

/**
 * Classe regroupant les initialisations des mod�les et de la vue.
 * Executez cette classe pour lancer le programme. 
 */
public class Main {

	public static void main(String[] args) {
		ReadMidi rm = new ReadMidi();
		ToPyboard toPyb = new ToPyboard();
		View view = new View(rm, toPyb);	
	}

}
