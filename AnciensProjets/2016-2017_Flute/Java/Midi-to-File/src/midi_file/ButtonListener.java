package midi_file;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * Classe g�rant les �v�nements qui surviennent lorsque l'on appuie sur un des boutons de l'interface graphique.
 * Cette classe impl�mente la classe ActionListener.
 */
public class ButtonListener implements ActionListener {
	
	private View view;
	
	/**
	 * Constructeur de la classe.
	 * 
	 * @param viewIn instance de la classe View qui �met les �v�nements
	 */
	public ButtonListener(View viewIn){
		this.view = viewIn;
	}
	
	/**
	 * M�thode appel�e d�s que la classe ButtonListener re�oit un �v�nement.
	 * Elle d�termine l'origine de l'�v�nement et r�agit en cons�quence.
	 * 
	 * @param e Ev�nement capt� lorsque l'utilisateur clique sur un bouton
	 */
	public void actionPerformed(ActionEvent e) {
		
		// Une fois qu'on rentre l'adresse du fichier .mid
		if(e.getActionCommand().equals("Valider")) {
			this.view.updateView2(this.view.getTextMidFile().getText());
		}
		
		// Pour fixer le BPM
		else if(e.getActionCommand().equals("Set BPM")) {
			int BPM = Integer.parseInt(this.view.getBPMText().getText());
			this.view.setBPM(BPM);
		}
		
		// Pour fixer le BPM
		else if(e.getActionCommand().equals("Set Target Repertory")) {
			this.view.setTarget(this.view.getTargetText().getText());
		}
		
		// Lorsque l'on souhaite sauvegarder les notes en fichier Python
		else if(e.getActionCommand().equals("Create Python File")) {
			this.view.createPyFile();
		}
		
		// Lorsque l'on souhaite sauvegarder les notes en fichier Texte
		else if(e.getActionCommand().equals("Create Txt File")) {
			this.view.createTxtFile();
		}
				
	}
}
