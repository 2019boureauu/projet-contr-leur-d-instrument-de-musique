 # main.py -- put your code here!
import pyb
import midi_reader as mr
from base import *
import time

# Ce dictionnaire contient les pas correspondant áchaque note
note_step = {
"A4" : 1049,
"A#4" : 949,
"B4" : 809,
"C5" : 747,
"C#5" : 641,
"D5" : 551,
"D#5" : 471,
"E5" : 401,
"F5" : 338,
"F#5" : 281,
"G5" : 223,
"G#5" : 174,
"A5" : 129,
"A#5" : 84,
"B5" : 40,
}

Tempo = 120 #tempo en BPM

#dictionnaire contenant les temps de trajets (prenant en compte la montée et la descente du piston) mesurés entre deux notes sous la forme temps_trajet[note1][note2]=temps de trajet pour aller de la note 1 à la note 2
Temps_trajet={"A4":{'A4':0,'A4#':30}}

note_de_ref='E2'
temps_descente=100 #temps de descente du piston en ms
temps_montée

#*******************************************************************************************************


# On initialise la communication USB série
serial = pyb.USB_VCP()
#on déclare le moteur du stystème de grattage
mot = pyb.Servo(1)
#On déclare la commande du distributeur
dist=0
"""à compléter"""
# On déclare les moteurs pour l'actionneur linéaire
pitch = Stepper(['X2','X3','X4','X5'])
# On fait la mise ázéro du moteur de la coulisse
pitch.zero_step('Y1')

#*******************************************************************************************************

partition=partition(mr.extract_notes('example.mid'))
notes=partition.get_notes()
print(notes)

 #converison des ticks en ms


#rejouter une fausse notes au début de la liste pour initialiser la position du chariot en bout de manche
#ajouter un évènement de fin de note
#vérification de la partition

t=0
current_note=1
last_current_note=0
t0=time.perf_counter_ns*1000
while current_note<=len(partition):
    if current_note=0:

    t=time.perf_counter_ns*1000-t0
    #si le temps écoulé depuis le début est superieur à l'instant auquel doit être joué la prochaine note moins le temps nécessaire pour déplacer
    # le chariot, on commence à le déplacer
    if notes[current_note][1]<t and dist='down' :
        dist='high'
        pyb.delay(temps_montée)
        current_note+=1
    elif notes[current_note][1]<t and dist='high' :
        pyb.LED(3).on()
        pitch.go_to_step(note_step[partition[current_note][2]])
    elif pitch.current_step==note_step[notes[current_note][2]] and dist='high':#le chariot est au bon endroit, on baisse le piston puis on gratte
        dist='down'
        """à modifier"""
        pyb.delay(temps_descente)
    if dist = 'down' and notes[current_note][0]<t: #actionnement du système de grattage si c'est le bon moment
            if mot.angle = 20:
                mot.angle(-20)
            else:
                mot.angle(20)
    if notes[current_note][1]<t:
        current_note+=1
    pyb.delay(1)

    notes[current_note][0]<t-Temps_trajet[notes[current_note][2]][notes[current_note-1][2]]-temps_descente

# On lance une boucle infinie
while(True):
# Si on reçoit des informations
    if(serial.any()):
    # On les décode et on les sépare suivant le caractère @
        data = serial.readline().decode('utf_8').strip()
        data = data.split("@")
        # On calcule le pas de la note reçue
        currentNote = note.get(data[0])
            if(len(data)==2 and (data[0] in note)):
            # Si on a bien reçu deux mots séparés par un @ et que le premier
            # est une note, on a bien en présence d'une requêtes du protocole
            pyb.LED(3).on()
                if(data[1]!="STOP"):
                    if dist='down' :
                        dist='high'
                        pyb.delay(temps_montée)
                        current_note+=1
                    elif dist='high' :
                        pyb.LED(3).on()
                        pitch.go_to_step(note_step[partition[current_note][2]])
                    elif pitch.current_step==note_step[notes[current_note][2]] and dist='high':#le chariot est au bon endroit, on baisse le piston puis on gratte
                        dist='down'
                        """à modifier"""
                        pyb.delay(temps_descente)
                    if dist = 'down' and notes[current_note][0]<t: #actionnement du système de grattage si c'est le bon moment
                            if mot.angle = 20:
                                mot.angle(-20)
                            else:
                                mot.angle(20)
                    # Si on a une note ájouer, on ouvre le flux d'air et on
                    # se place pour jouer la note
                    mot.angle(20)

                    pitch.go_to_step(currentNote)

                    """currentVel = int(data[1]) # Implémentation
                    ultérieure"""
                else:
                # Sinon on referme le flux d'air
                    mot.angle(0)

                """currentVel = data[1] # Implémentation
                ultérieure"""

                    # Laisse un petit peu de temps entre chaque boucle
                pyb.delay(1)
        