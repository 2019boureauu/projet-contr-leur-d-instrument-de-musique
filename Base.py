import pyb

class Stepper:
"""
Cette classe permet de contrôler le moteur pas-á-pas
"""
# Liste de l'ordre des pas áeffectuer pour la rotation du moteur
steps = [
[1],
[1, 2],
[2],
[2, 3],
[3],
[3, 4],
[4],
[1, 4]
]

def __init__(self, pins):
    # On initialise le moteur avec les pins ou sont connectées notre pont en H
    self.pins = [pyb.Pin(pin, pyb.Pin.OUT_PP) for pin in pins]
    # On initialise les pas maximum, le temps d'attente du moteur, etc...
    self.current_step = 0
    self.step_counter = 0
    self.max_step = 1060 # Mesuré exprimentalement
    self.delay_us = 500

def do_step(self,n=1):
# Fait n pas dans un sens ou dans l'autre

    direction = (n>0) - (n<0)
    for _ in range(abs(n)):
        self._low_on_all()
        self._high_on_step_pins()
        self._record_step(direction)
        pyb.udelay(self.delay_us)

def _low_on_all(self):
    # Méthode annexe pour simplifier la compréhension du code
    for pin in self.pins:
        pin.low()

def _high_on_step_pins(self):
    # Méthode annexe pour simplifier la compréhension du code
    high_pins = self.steps[self.current_step]
    for pin_number in high_pins:
        self.pins[pin_number - 1].high()

def _record_step(self,direction):
    # Enregistre les pas effectues pour le controle du moteur et
    # connaitre l'emplacement actuel
    self.current_step += direction*1
    self.step_counter += direction*1
    if self.current_step == direction*len(self.steps):
        self.current_step = 0

def zero_step(self,pinName='Y1'):
    # Fonction de mise ázero
    # La pin zero_pin est la pin de connexion du capteur de fin de course
    zero_pin = pyb.Pin(pinName,pyb.Pin.IN, pyb.Pin.PULL_DOWN)
    while(not zero_pin.value()):
        self.do_step(-1)
        self.step_counter = 0

def get_current_step(self):
# Donne la position actuelle du moteur
    return self.step_counter

def set_max_step(n):
# On peut définir notre pas maximum
    self.max_step = n

def go_to_step(self,n):
    # Se deplacer jusqu'á la position souhaitée
    if (n>=0 and n<=self.max_step):
        self.do_step(n-self.step_counter)