import tkinter as tk
import time

fen_princ = tk.Tk()

fen_princ.title("ESSAI AVEC CANVAS")

fen_princ.geometry("700x600")

cases = {1:85, 3:220, 4:275, 5:320}
notes = {"Fa":1, "Sol":3, "La":5}

monCanvas = tk.Canvas(fen_princ, width=600, height=500, bg='ivory')

monCanvas.pack()

monCanvas.place(x=50,y=50)
monCanvas.create_line(50, 250, 530, 250, width=2, fill="black")
monCanvas.create_line(50, 350, 530, 350, width=2, fill="black")
monCanvas.create_line(50, 250, 50, 350, width=2, fill="black")
monCanvas.create_line(530, 250, 530, 350, width=2, fill="black")
monCanvas.create_line(125, 250, 125, 350, width=2, fill="black")
monCanvas.create_line(190, 250, 190, 350, width=2, fill="black")
monCanvas.create_line(250, 250, 250, 350, width=2, fill="black")
monCanvas.create_line(300, 250, 300, 350, width=2, fill="black")
monCanvas.create_line(340, 250, 340, 350, width=2, fill="black")
monCanvas.create_line(370, 250, 370, 350, width=2, fill="black")
monCanvas.create_line(405, 250, 405, 350, width=2, fill="black")
monCanvas.create_line(435, 250, 435, 350, width=2, fill="black")
monCanvas.create_line(463, 250, 463, 350, width=2, fill="black")
monCanvas.create_line(488, 250, 488, 350, width=2, fill="black")
monCanvas.create_line(510, 250, 510, 350, width=2, fill="black")
rayon=10
#x,y=cases[1],300
x,y=cases[5],300
color="black"
Chariot = monCanvas.create_oval(x-rayon,y-rayon,x+rayon,y+rayon, fill=color)
dx=5
dist='high'
mot='left'

def play(note) :
  global x,dx
  dest = cases[notes[note]]
  if x == dest:
    return
  if x < dest :
    dx = 5
  else:
    dx = -5
  deplacement(dx, dest)

def red():
  monCanvas.itemconfigure(Chariot,fill='red',width=0)

def black():
  monCanvas.itemconfigure(Chariot,fill='black',width=0)

def green():
  monCanvas.itemconfigure(moteur,fill='green',width=0)

def blue():
  monCanvas.itemconfigure(moteur,fill='blue',width=0)


def push():
  global dist
  if dist == 'high':
    monCanvas.after(0,red)
    dist = 'down'
  elif dist == 'down':
    monCanvas.after(0,black)
    dist = 'high'

def gratte():
  global mot
  if mot == 'left':
    monCanvas.after(0,green)
    mot = 'right'
  elif mot == 'right':
    monCanvas.after(0,blue)
    mot = 'left'


  
  
def deplacement(dx,dest):
    global x,mot
    #On deplace le chariot :
    x=x+dx
    monCanvas.coords(Chariot,x-rayon,y-rayon,x+rayon,y+rayon)
#     On repete cette fonction
    print(x)
    if (dx > 0 and x < dest) or (dx < 0 and x > dest) :
      if dist == 'down':
        push()
      monCanvas.after(100,deplacement,dx,dest)
    if x == dest :
      if dist == 'high':
        push()
        print(dist)
      gratte()

color="black"
moteur = monCanvas.create_oval(530,y-rayon,550,y+rayon, fill='blue')

def Sol():
    global x,y
    if x > cases[3]:
#         dx=-5
#         while x>220:
        deplacement(-5, 220)
        m=3
    if x < cases[3]:
#         dx=5
#         while x<220:
#             deplacement(dx)
        deplacement(5, 220)
#    m=3
"""    monCanvas.after(Chariot,couleur)
    monCanvas.after(moteur, couleur)
def couleur():
    global color
    color="red" """
#Sol()
fen_princ.after(0, play, "Sol")
fen_princ.after(3000, play, "La")
fen_princ.after(6000, play, "Fa")

fen_princ.mainloop()