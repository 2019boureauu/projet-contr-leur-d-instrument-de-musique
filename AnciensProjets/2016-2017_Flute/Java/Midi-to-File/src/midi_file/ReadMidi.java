package midi_file;
	
import java.io.File;
import java.util.ArrayList;

import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Track;

/**
 * Mod�le du fichier midi.
 * Les attributs de cette classe contiennent toutes les informations utiles 
 * pouvant �tre extraites d'un fichier midi.
 * 
 * Ces attribus sont initialis�s via la m�thode initReadMidi. On peut ensuite 
 * les r�cup�rer gr�ce aux getters.
 */
public class ReadMidi {
	
	private int trackNumber;
	private ArrayList<Integer> trackEndIndex;
	private ArrayList<Integer> commandTable;
	private ArrayList<Integer> timeTable;
	private ArrayList<Integer> velocityTable;
	private ArrayList<Integer> keyTable;
	private int res;
	
	/**
	 * Cette m�thode initialise les variables trackNumber, trackEndIndex, commandTable, 
	 * timeTable, velocityTable, keyTable et res.
	 * 
	 * @param path chemin d'acc�s au fichier midi (.mid)
	 * @throws Exception exception lanc�e si le chemin sp�cifi� ne correspond a aucun fichier midi
	 */
	public void initReadMidi(String path) throws Exception {
		// On teste si le chemin sp�cifi� par l'utilisateur ainsi que l'extention du fichier sont correct
		if(new File(path).exists() && path.substring(path.length()-4).equals(".mid")) {
			// On ouvre le fichier midi
			Sequence sequence = MidiSystem.getSequence(new File(path));
			
			// Informations g�n�rales sur le fichier midi
			this.trackNumber = 0;
			this.trackEndIndex = new ArrayList<Integer>();
			this.res = sequence.getResolution(); // La r�solution temporelle est primordiale pour convertir les 'ticks' en secondes.
			
			// Tableaux contenant des informations sur les notes
			this.commandTable = new ArrayList<Integer>();
			this.timeTable = new ArrayList<Integer>();
			this.velocityTable = new ArrayList<Integer>();
			this.keyTable = new ArrayList<Integer>();			
			
			for (Track track :  sequence.getTracks()) {
				
				this.trackNumber++;
				this.trackEndIndex.add(-1);
	
				for (int i=0; i < track.size(); i++) { 
					
					MidiEvent event = track.get(i);
					MidiMessage message = event.getMessage();
					
					if (message instanceof ShortMessage) {
						// On actualise la longueur du track
						this.trackEndIndex.set(this.trackNumber-1, this.trackEndIndex.get(this.trackNumber-1)+1);
						
						// Temps de la note
						long tick = event.getTick();
						Integer time = (int) (long) tick;
						this.timeTable.add(time); 
						
						// On r�cup�re le message de la note
						ShortMessage sm = (ShortMessage) message;
						
						// Note
						int key = sm.getData1();
						this.keyTable.add(key);
						
						// V�locit� de la note
						int velocity = sm.getData2();
						this.velocityTable.add(velocity);
						
						// Type de commande de la note
						int command = sm.getCommand();
						this.commandTable.add(command);
					}
				}
			}
		}
		else {
			throw new Exception("Fichier Midi introuvable");
		}
	}
	
	/**
	 * Getter retournant le nombre de tracks dans le fichier midi.
	 * 
	 * @return trackNumber
	 */
	public int getNumberTracks() {
		return(this.trackNumber);
	}
	
	/**
	 * Getter retournant l'index de fin de chaque track pour les listes keyTable, 
	 * timeTable, commandTable et velocityTable.
	 * 
	 * @return trackEndIndex liste des index de fin
	 */
	public ArrayList<Integer> getTrackEndIndex() {
		return(this.trackEndIndex);
	}
	
	/**
	 * Getter retournant la liste des notes du fichier midi.
	 * Les notes sont cod�es par un nombre variant de 0 (note la plus grave) 
	 * � 127 (note la plus aigue).
	 * 
	 * @return keyTable liste des notes
	 */
	public ArrayList<Integer> getKeyTable() {
		return(this.keyTable);
	}
	
	/**
	 * Getter retournant la liste des temps (en r�f�rentiel absolu) auxquels 
	 * surviennent les �v�nements midi enregistr�s.
	 * 
	 * @return timeTable liste des temps
	 */
	public ArrayList<Integer> getTimeTable() {
		return(this.timeTable);
	}
	
	/**
	 * Getter retournant la liste des types de commandes de chaque �v�nement midi
	 * enregistr�.
	 * 
	 * @return commandTable liste des commandes
	 */
	public ArrayList<Integer> getCommandTable() {
		return(this.commandTable);
	}
	
	/**
	 * Getter retournant la liste des v�locit�s de chaque �v�nement midi enregistr�.
	 * 
	 * @return velocityTable liste des v�locit�s
	 */
	public ArrayList<Integer> getVelocityTable() {
		return(this.velocityTable);
	}
	
	/**
	 * Getter retournant la r�solution temporelle de l'enregistrement midi.
	 * Cette r�solution est tr�s importante pour interpr�ter la liste timeTable.
	 * 
	 * @return res resolution temporelle
	 */
	public int getRes() {
		return(this.res);
	}
	
}
			
		
