import asyncio

####
# On utilise asyncio pour avoir plusieurs tâches qui s'exécutent en même temps, 
# mais de manière coopérative. Aini, une tâche ne cède du temps aux autres que 
# volontairement, quand elle attend autre chose, grâce à "await"
# Ces tâches sont déclarées "async" de façon à pouvoir être gérées par un ordonnanceur 
# de tâche qui est mis en œuvre par asyncio.run().
#
# La communication entre les tâches se fait en mode client-serveur. On crée un
# serveur pour chaque tâche, et ceux qui veulent utiliser le résultat de la tâche
# se connectent au serveur.
###

"""
Cette fonction lit le contenu d'un pseudo fichier MIDI (ici, il ne contient
que des paires (noms de notes, durée)) et l'écrit sur un flux de sortie, en attendant
la durée entre chaque note. L'idée est de simuler un séquenceur ou un clavier midi.

filename est le nom du fichier à lire, writer est le flux de sortie sur lequel
envoyer les notes lues.
"""
async def readMIDI(filename, writer) :
  # On ouvre le fichier, qui sera fermé automatiquement en sortant du "with"
  with open(filename) as midifile :
    # On lit une ligne du fichier (une note par ligne pour simplifier)
    l = midifile.readline()
    # Tant que le fichier n'est pas fini
    while len(l) > 0 :
      print("#MIDI processing " + l)
      # on supprime les blancs au début et à la fin de la ligne (strip)
      # et on sépare la chaîne en une liste de mots (split)
      l = l.strip().split()
      # On écrit la note lue (1er élément) sur le flux de sortie.
      # On ajoute "\r\n" pour avoir une fin de ligne partout (Mac, Windows, Linux)
      # La méthode "write" attend des bytes, il faut donc encoder a chaîne de caractères avec encode()
      writer.write((l[0]+"\r\n").encode())
      # On attend que l'écriture soit faite
      await writer.drain()
      # On attend la durée (2e élément, en millisecondes, donc on divise par 1000)
      await asyncio.sleep(int(l[1])/1000)
      # On lit la ligne suivante, et on recommence la boucle
      l = midifile.readline()
    # Quand on a traité tout le fichier, on ferme le flux de sortie
    writer.close()
    # Et on attend qu'il soit bien fermé
    await writer.wait_closed()

"""
Cette fonction traite les connexions au serveur qui produit les notes en lisant le fichier MIDI
"""
async def handle_midi(midireader, midiwriter) :
  # La tâche est créée pour exécuter readMIDI en lisant le fichier toto.mid
  # et en envoyant le résultat sur le flux midiwriter
  asyncio.create_task(readMIDI("toto.mid", midiwriter))


if __name__ == "__main__":
  """
  Ce programme de test crée un serveur sur la machine (127.0.0.1 est l'adresse de la 
  machine locale) qui écoute les requêtes de connexion sur le port 10000.
  Quand une connexion est demandée, la fonction handle_midi est appelée.

  Le programme se connecte ensuite à ce serveur, lit chacune des lignes produites
  par le serveur et les affiche dans la console.
  """
  async def main() :
    # Lancement du serveur sur le port 10000
    await asyncio.start_server(handle_midi, host="127.0.0.1", port=10000)
    # Connexion au serveur, ce qui rend une paire de flux permettant 
    # de recevoir et d'envoyer des données au serveur
    (midireader, midiwriter) = await asyncio.open_connection(host="127.0.0.1", port=10000)
    # On lit une ligne depuis le serveur
    l = await midireader.readline()
    # Si on a lu quelque chose
    while not (l is None or len(l) == 0) :
      # On l'affiche sur la console
      print("Read '"+l.decode().strip()+"'")
      # On lit la ligne suivante
      l = await midireader.readline()

  # On exécute le programme de test comme une tâche asyncio
  asyncio.run(main())
