Ce dossier contient des exemples de communications entre tâches.
  - readmidi.py est une tâche qui lit le fichier toto.mid (attention, 
    c'est un faux fichier MIDI, il ne contient que des notes, une par ligne, 
    en notation anglosaxonne)
  - pyboardcontrol.py est une tâche qui traduit les nom de notes anglosaxons 
    en nom de note à l'italienne
  - bassguitar.py est une tâche qui traduit les notes italiennes en numéro de case
  - systemecomplet.py est l'assemblage de ces trois tâches pour convertir un fichier 
    midi en déplacements sur le manche de la guitare

Les fonctionnalités de ces tâches sont pipo, ce ne sont que des exemples pour vous 
montrer comment définir une tâche, créer un serveur qui l'exécute et vous connecter 
à ce serveur.

Vous pouvez exécuter chacun des fichiers readmidi.py, pyboardcontrol.py et bassguitar.py 
car ils contiennent un main pour tester le fonctionnement de leur tâche.

J'ai essayé de commenter le code en espérant que cela vous permettra de comprendre 
ce qui se passe.
