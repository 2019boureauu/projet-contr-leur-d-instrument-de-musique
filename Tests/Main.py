 # main.py -- put your code here!
import pyb
# Ce dictionnaire contient les pas correspondant áchaque note
note = {
"A4" : 1049,
"A#4" : 949,
"B4" : 809,
"C5" : 747,
"C#5" : 641,
"D5" : 551,
"D#5" : 471,
"E5" : 401,
"F5" : 338,
"F#5" : 281,
"G5" : 223,
"G#5" : 174,
"A5" : 129,
"A#5" : 84,
"B5" : 40,
}

# On initialise la communication USB série
serial = pyb.USB_VCP()
# On déclare les moteurs pour la coulisse (pitch, pas-á-pas) et
# le contrpole du flux d'air (mot, servomoteur)
mot = pyb.Servo(1)
pitch = Stepper(['X2','X3','X4','X5'])
# On fait la mise ázéro du moteur de la coulisse
pitch.zero_step('Y1')

# On lance une boucle infinie
while(True):
# Si on reçoit des informations
    if(serial.any()):
    # On les décode et on les sépare suivant le caractère @
        data = serial.readline().decode('utf_8').strip()
        data = data.split("@")
        # On calcule le pas de la note reçue
        currentNote = note.get(data[0])
            if(len(data)==2 and (data[0] in note)):
            # Si on a bien reçu deux mots séparés par un @ et que le premier
            # est une note, on a bien en présence d'une requêtes du protocole
            pyb.LED(3).on()
                if(data[1]!="STOP"):
                    # Si on a une note ájouer, on ouvre le flux d'air et on
                    # se place pour jouer la note
                    mot.angle(20)

                    pitch.go_to_step(currentNote)

                    """currentVel = int(data[1]) # Implémentation
                    ultérieure"""
                else:
                # Sinon on referme le flux d'air
                    mot.angle(0)

                """currentVel = data[1] # Implémentation
                ultérieure"""

                    # Laisse un petit peu de temps entre chaque boucle
                pyb.delay(1)