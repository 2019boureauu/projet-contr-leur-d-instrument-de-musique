import asyncio

####
# On utilise asyncio pour avoir plusieurs tâches qui s'exécutent en même temps, 
# mais de manière coopérative. Aini, une tâche ne cède du temps aux autres que 
# volontairement, quand elle attend autre chose, grâce à "await"
# Ces tâches sont déclarées "async" de façon à pouvoir être gérées par un ordonnanceur 
# de tâche qui est mis en œuvre par asyncio.run().
#
# La communication entre les tâches se fait en mode client-serveur. On crée un
# serveur pour chaque tâche, et ceux qui veulent utiliser le résultat de la tâche
# se connectent au serveur.
###

"""
Cette fonction lit des données ligne par ligne sur un flux d'entrée,
les interprète comme des nom de note, et écrit des numéros son flux de sortie.
L'idée est de simuler le comportement du système de contrôle de la guitare qui
reçoit des commandes et déplace le chariot sur une case du manche.
"""
async def bassguitar(reader, writer) :
  # On lit une ligne sur le flux d'entrée
  l = await reader.readline()
  # Tant qu'on lit quelque chose
  while not ((l is None) or (len(l) == 0)) :
    # On décode les données brutes (bytes) pour obtenir une chaîne de caractères
    # et on supprime les blancs en tête et en fin de chaîne
    l = l.decode().strip()
    # On reconnaît la note et on écrit le numéro de la case sur le flux de sortie.
    # On ajoute "\r\n" pour avoir une fin de ligne quelle que soit la plateforme
    # (Mac, Windows, Linux), et on utilise encode() pour transformer la chaîne
    # de caractères en bytes (c'est ce qu'attend la méthode write() du flux)
    if l == "La" :
      writer.write("#1\r\n".encode());
      await writer.drain()
    elif l == "Si" :
      writer.write("#2\r\n".encode());
      await writer.drain()
    elif l == "Do" :
      writer.write("#3\r\n".encode());
      await writer.drain()
    elif l == "Ré" :
      writer.write("#4\r\n".encode());
      await writer.drain()
    elif l == "Mi" :
      writer.write("#5\r\n".encode());
      await writer.drain()
    elif l == "Fa" :
      writer.write("#6\r\n".encode());
      await writer.drain()
    elif l == "Sol" :
      writer.write("#7\r\n".encode());
      await writer.drain()
    else :
      print("# Error, unknown note: " + l)
    # On lit la ligne suivante et on reboucle
    l = await reader.readline()
  # Quand c'est fini, on ferme le flux de sortie pour indiquer au suivant qu'il n'y a plus rien à lire.
  writer.close()
  await writer.wait_closed()

"""
Cette fonction traite les connexions au serveur qui traduit les notes en positions
"""
async def handle_guitar(bassreader, basswriter) :
	asyncio.create_task(bassguitar(bassreader, basswriter))

if __name__ == "__main__":
  """
  Ce programme de test crée un serveur sur la machine (127.0.0.1 est l'adresse de la 
  machine locale) qui écoute les requêtes de connexion sur le port 10000.
  Quand une connexion est demandée, la fonction handle_guitar est appelée.

  Le programme se connecte ensuite à ce serveur, lit chacune des lignes produites
  par le serveur et les affiche dans la console.
  """
  async def main() :
    # Lancement du serveur sur le port 10000
    await asyncio.start_server(handle_guitar, host="127.0.0.1", port=10000)
    # Connexion au serveur, ce qui rend une paire de flux permettant 
    # de recevoir et d'envoyer des données au serveur
    (bassreader, basswriter) = await asyncio.open_connection(host="127.0.0.1", port=10000)
    # Données utilisées pour tester le programme
    input = ("La", "Si", "Do", "Ré", "Mi", "Fa", "Sol")
    # Résultats obtenus
    result = []
    # Pour chaque donnée à tester
    for n in input :
      # On envoie la donnée au serveur (avec fin de ligne pour que readline marche bien)
      basswriter.write((n + "\r\n").encode())
      # On attend que les données soient parties
      await basswriter.drain()
      # On lit la réponse du serveur
      l = await bassreader.readline()
      # Et on l'ajoute (décodée et strippée) au tableau des résultats
      result.append(l.decode().strip())
    # Quand c'est fini, on affiche sur la console les données envoyées et les résultats obtenus.
    print(str(input) + "\r\n--> " + str(result))

  # On exécute le programme de test comme une tâche asyncio
  asyncio.run(main())
