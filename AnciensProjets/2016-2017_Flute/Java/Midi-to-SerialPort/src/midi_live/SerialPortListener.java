package midi_live;

import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

/**
 * Listener captant si un message est transmis par le dispositif d'un port s�rie.
 * Impl�mente la classe SerialPortEventListener de RXTX.
 */
public class SerialPortListener implements SerialPortEventListener {
	
	private ManagePorts mp;
	private View view;
	private String logTxt;
	
	/**
	 * Constructeur de la classe.
	 * Etabli le lien avec le mod�le du port s�rie et la vue.
	 * 
	 * @param mpIn mod�le du port s�rie � lier � cette classe
	 * @param viewIn vue � lier � cette classe
	 */
	public SerialPortListener(ManagePorts mpIn, View viewIn) {
		this.mp = mpIn;
		this.view = viewIn;
		this.logTxt = "";
	}
	
	/**
	 * M�thode appel�e d�s que le listener intercepte un message provenant du dispositif du port s�rie.
	 * Retransmet ce message � la vue, en l'inscrivant sur la console de log.
	 * 
	 * @param evt �v�nement g�n�r� par la r�ception d'un message
	 */
	public void serialEvent(SerialPortEvent evt) {
        if (evt.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
			try {
                byte singleData = (byte) this.mp.getInput().read();
                if(singleData != Main.NEW_LINE_ASCII) {
                	this.logTxt += new String(new byte[] {singleData});
                }
                else {
                	this.view.appendLog("[COM_Out] : " + this.logTxt, false);
                	this.logTxt = "";
                }
            }
            catch (Exception e) {
            	e.printStackTrace();
                this.view.setError("Erreur lors de la lecture des donn�es entrantes. (" + e.toString() + ")");
            }
        }
    }

}
