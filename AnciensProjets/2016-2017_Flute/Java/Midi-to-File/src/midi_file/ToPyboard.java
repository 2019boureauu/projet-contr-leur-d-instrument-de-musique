package midi_file;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import javax.sound.midi.ShortMessage;

/**
 * Mod�le du fichier � destination de la Pyboard.
 * Cette classe interprete les donn�es lues par ReadMidi afin de
 * cr�er un fichier contenant les informations n�cessaires facilement
 * lisibles par la PyBoard.
 */
public class ToPyboard {
	
	private String[] NOTE_NAMES;
	private int BPM;
	private String target;
	private ArrayList<ArrayList<String>> notes;
	
	/** 
	 * Constructeur de la classe.
	 * Initialise la correspondance entre numero et nom de la note.
	 */
	public ToPyboard() {
		NOTE_NAMES = new String[]{"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"};
	}
	
	/**
	 * Cette m�thode r�cup�re les informations fournies par ReadMidi afin d'initialiser 
	 * l'attribut notes qui contient la liste des noms des notes, leur octave
	 * et leur dur�e dans le temps.
	 * Il faut initialiser l'attribut BPM avant de lancer cette m�thode.
	 * 
	 * @param rm instance de ReadMidi contenant les informations du fichier midi
	 */
	public void initNotes(ReadMidi rm) {
		// Variable a completer
		this.notes = new ArrayList<ArrayList<String>>();
		
		// Donn�es dont on dispose
		ArrayList<Integer> keyTable = rm.getKeyTable();
		ArrayList<Integer> commandTable = rm.getCommandTable();
		ArrayList<Integer> timeTable = rm.getTimeTable();
		
		// Variables temporaires
		ArrayList<Integer> keyOn = new ArrayList<Integer>();
		ArrayList<Integer> indexKeyOn = new ArrayList<Integer>();
		int lastOff = 0;
		
		// Facteur de proportionalit� entre ticks et secondes
		float secPerTick = 60000 / (this.BPM * rm.getRes()); // Millisecondes par Tick
		secPerTick = secPerTick / 1000; // Secondes par Tick
		
		for(int i=0 ; i < keyTable.size() ; i++) {
			// On r�cup�re la note
			Integer key = keyTable.get(i);
			
			// Message de type NOTE_ON
			if(commandTable.get(i) == ShortMessage.NOTE_ON) {
				if(!keyOn.contains(key)) {
					// Si aucune note �tait jou�e, il faut prendre en compte le silence.
					if(keyOn.isEmpty() && timeTable.get(i)!=lastOff) {
						ArrayList<String> listSilence = new ArrayList<String>();
						listSilence.add("OFF"); // "OFF" est le message caracterisant un silence
						listSilence.add("0");
						double temps = secPerTick * (timeTable.get(i) - lastOff);
						listSilence.add(Double.toString(temps));
						this.notes.add(listSilence);
					}
					
					// Ajout de la note (le temps sera pr�cis� lorsqu'on recuperera le message NOTE_OFF)
					ArrayList<String> listNote = new ArrayList<String>();
					int note = key % 12;
					int octave = (key / 12)-1;
					String noteName = this.NOTE_NAMES[note];
					listNote.add(noteName);
					listNote.add(Integer.toString(octave));
					listNote.add(timeTable.get(i).toString()); // On garde en m�moire le temps NOTE_ON
					this.notes.add(listNote);
					
					// On marque la note comme �tant ON				
					keyOn.add(key);
					indexKeyOn.add(this.notes.size()-1); // On garde en m�moire l'indice de la note dans notesArray	
				}
			}
			
			// Message de type NOTE_OFF
			else if(commandTable.get(i) == ShortMessage.NOTE_OFF) {
				if(keyOn.contains(key)) {
					// On r�cup�re l'indice de la note que l'on �teind				
					int n = keyOn.indexOf(key);
					int m = indexKeyOn.get(n);
					
					// Calcul temps de la note
					double temps = secPerTick  * (timeTable.get(i) - Integer.parseInt(this.notes.get(m).get(2)));
					
					// Insertion du temps de la note
					this.notes.get(m).set(2, Double.toString(temps));
					 
					// On indique le dernier instant pendant lequel une note jou�e a �t� �teinte.
					lastOff = timeTable.get(i);
					 
					// On enl�ve la note des notes ON
					keyOn.remove(n);
					indexKeyOn.remove(n);
				}
			}
		}
	}
	
	/**
	 * Getter retournant la liste des noms des notes, leur octave et leur dur�e.
	 * 
	 * @return notes liste des notes
	 */
	public ArrayList<ArrayList<String>> getNotes() {
		return(this.notes);
	}
	
	/**
	 * M�thode retournant une repr�sentation de l'attribut notes sous forme de liste Python.
	 * 
	 * @return s chaine de caractere de la repr�sentation de notes en liste Python
	 */
	public String notesToString() {
		int l = this.notes.size();
		
		String s = "data = [";
	    for (int i=0 ; i < l-1 ; i++){
	    	s += "[\""+ this.notes.get(i).get(0) + "\"," + this.notes.get(i).get(1) + "," + this.notes.get(i).get(2) + "], ";
	    }
	    s += "[\""+ this.notes.get(l-1).get(0) + "\"," + this.notes.get(l-1).get(1) + "," + this.notes.get(l-1).get(2) + "]]";
	    return(s);
	}
	
	/**
	 * Setter permettant de r�gler l'attribut BPM.
	 * 
	 * @param BPMIn valeur du BPM
	 */
	public void setBPM(int BPMIn) {
		this.BPM = BPMIn;
	}
	
	/**
	 * Setter permettant de fixer le dossier de sauvegarde des fichiers.
	 * 
	 * @param targetIn chemin du dossier de sauvegarde
	 * @throws Exception exception lanc�e si le chemin sp�cifi� est invalide
	 */
	public void setTarget(String targetIn) throws Exception {
		if(targetIn.equals("") || targetIn.substring(targetIn.length()-1).equals("\\")) {
			this.target= targetIn;
		}
		else {
			throw new Exception("Chemin sp�cifi� invalide.");
		}
	}
	
	/**
	 * M�thode g�n�rant un fichier .py contenant les donn�es de l'attribut 
	 * notes sous forme de liste Python.
	 */
	public void toPyFile() {
		try {
		    FileWriter fstream = new FileWriter(this.target + "notes.py"); // Create file
		    BufferedWriter out = new BufferedWriter(fstream);
		    
		    // On ins�re les donn�es dans le fichier
		    out.write(this.notesToString());
		    out.close();
		    
		} catch (IOException e) {
			System.err.println("Error: " + e.getMessage());
		} 
	}
	
	/**
	 * M�thode g�n�rant un fichier .txt contenant les donn�es de l'attribut notes
	 * pars�s par ";" et ",".
	 */
	public void toTxtFile() {
		try {
		    FileWriter fstream = new FileWriter(this.target + "notes.txt"); // Create file
		    BufferedWriter out = new BufferedWriter(fstream);
		    
		    // On ins�re les donn�es dans le fichier
		    int l = this.notes.size();
			
			String s = "";
		    for (int i=0 ; i < l-1 ; i++){
		    	s += this.notes.get(i).get(0) + "," + this.notes.get(i).get(1) + "," + this.notes.get(i).get(2) + ";";
		    }
		    s += this.notes.get(l-1).get(0) + "," + this.notes.get(l-1).get(1) + "," + this.notes.get(l-1).get(2);
		    
		    out.write(s);
		    out.close();
		    
		} catch (IOException e) {
			System.err.println("Error: " + e.getMessage());
		} 
	}
	
}
