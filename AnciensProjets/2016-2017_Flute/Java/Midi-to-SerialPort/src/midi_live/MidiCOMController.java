package midi_live;

import java.util.ArrayList;

import javax.sound.midi.ShortMessage;

/**
 * Controlleur liant les mod�les ManagePorts et ManageMidi.
 */
public class MidiCOMController {
	
	private ManagePorts mp;
	private ManageMidi mm;
	private ArrayList<Integer> keyOn;
	
	/**
	 * Constructeur de la classe.
	 * 
	 * @param mpIn mod�le du port s�rie � lier
	 * @param mmIn mod�le du dispositif midi � lier
	 */
	public MidiCOMController(ManagePorts mpIn, ManageMidi mmIn) {
		this.mp = mpIn;
		this.mm = mmIn;
		this.keyOn = new ArrayList<Integer>();
	}
	
	
	/**
	 * Traite les messages midi transmis par le mod�le ManageMidi (m�thode midiToNote) pour les renvoyer au mod�le ManagePorts (m�thode writeData).
	 * Le message transmis au dispositif du port s�rie � travers la m�thode writeData est de la forme
	 * "note([nom_de_la_note, octave_de_la_note, dur�e_de_la_note = 0])".
	 * 
	 * @param msg message midi entrant
	 */
	public void midiToNote(ShortMessage msg) {
		
		Integer key = msg.getData1();

		if(msg.getCommand() == ShortMessage.NOTE_ON) {
			// Si aucune autre note est jou�e, on joue la note
			if(this.keyOn.isEmpty()) {
				int note = key % 12;
				int octave = (key / 12)-1;
				String noteName = Main.NOTE_NAMES[note];
				String pybCommand = "note([\"" + noteName + "\", " + octave + ", 0])";
				this.mp.writeData(pybCommand);
			}
			// On ajoute la note aux notes jou�es
			this.keyOn.add(key);
		}
		else if(msg.getCommand() == ShortMessage.NOTE_OFF) {
			// On enl�ve la note des notes jou�es
			int n = keyOn.indexOf(key);
			this.keyOn.remove(n);
			
			// Si plus aucune note est jou�e, on arr�te
			if(this.keyOn.isEmpty()) {
				String pybCommand = "note([\"OFF\", 0, 0])";
				this.mp.writeData(pybCommand);
			}
			// Sinon on joue la note suivante parmis les notes jou�es
			else {
				Integer nextKey = this.keyOn.get(0);
				int note = nextKey % 12;
				int octave = (nextKey / 12)-1;
				String noteName = Main.NOTE_NAMES[note];
				String pybCommand = "note([\"" + noteName + "\", " + octave + ", 0])";
				this.mp.writeData(pybCommand);
			}	
		}
	}

}
