import asyncio
import midi
import time


notes_dic={40:'E2',41:'F2',42:'F#2',43:'G2',44:'G#2',45:'A2',46:'A#2',47:'B2',48:'C3',49:'C#3',50:'D3',51:'D#3'}

Tempo = 120 #tempo en BPM

#dictionnaire contenant les temps de trajets (prenant en compte la montée et la descente du piston) mesurés entre deux notes sous la forme temps_trajet[note1][note2]=temps de trajet pour aller de la note 1 à la note 2
Temps_trajet={"E2":
{"E2" : 0,
"F2" : 10,
"F#2" : 10,
"G2" : 10,
"G#2" : 10,
"A2" : 10,
"A#2" : 10,
"B2" : 10,
"C3" : 10,
"C#3" : 10,
"D3" : 10,
"D#3" : 10}, 
"F2" :
{"E2" : 10,
"F2" : 0,
"F#2" : 10,
"G2" : 10,
"G#2" : 10,
"A2" : 10,
"A#2" : 10,
"B2" : 10,
"C3" : 10,
"C#3" : 10,
"D3" : 10,
"D#3" : 10,
"F#2" : 10,
"G2" : 10,
"G#2" : 10,
"A2" : 10,
"A#2" : 10,
"B2" : 10,
"C3" : 10,
"C#3" : 10,
"D3" : 10,
"D#3" : 10}
#à compléter
}

note_de_ref='E2'
temps_descente=100 #temps de descente du piston en ms
temps_montée=100


class partition:
    """On traite la partition comme un objet. Il faut faire attention au fait que la caractéristique 'Notes' contient les temps en ticks, pour avoir
    accès aux notes, il faut faire partition.get_notes"""

    def __init__(self,fichier):
        pattern = midi.read_midifile(fichier)
        Notes=[]
        time=0
        for event in pattern[2]:
            if event.name == 'Note On':
                time+=event.tick
                Notes.append([time,0,notes_dic[event.data[0]]])
            if event.name == 'Note Off' and notes_dic[event.data[0]]==Notes[-1][2] :
                time+=event.tick
                Notes[-1][1]=time
            elif event.name == 'Note on' and Notes[-1][2] == 0:
                return 'Erreur : le fichier n\'est pas jouable sur une seule corde'
        self.resolution=pattern.resolution
        self.tempo=80
        self.notes=Notes


    def get_notes(self):
        ticks_to_ms=60*1000/(self.tempo*self.resolution)
        notes_en_ms=[]
        for i in range(0,len(self.notes)):
            notes_en_ms.append([0,0,0])
            notes_en_ms[i][0]=self.notes[i][0]*ticks_to_ms
            notes_en_ms[i][1]=self.notes[i][1]*ticks_to_ms
            notes_en_ms[i][2]=self.notes[i][2]
        return notes_en_ms

def check_notes(notes):
    #à compléter
    return


#partition=partition('demo1.mid')

# notes120=partition.get_notes()
# partition.tempo=110
# notes110=partition.get_notes()
# print(notes120,notes110)

async def readMIDI(filename, writer) :
    # On lit une ligne du fichier (une note par ligne pour simplifier)
    l = partition(filename).get_notes()
    # On va soustraire aux temps de jeu les temps de déplacement du robot
    #pour la première note, on considère que le chariot est initialement à une position de référence
    l[0][0]+=0 #-temps_descente-Temps_trajet[l[0][2]][note_de_ref]
    for i in range(1,len(l)):
        l[i][0]+=0 #-temps_descente-temps_montée #-Temps_trajet[l[i-1][2]][l[i][2]]
    check_notes(l)    
    current_note=0
    note_en_cours=False
    #on définit le temps en soustrayant 2 secondes pour que le robbot ait le temps de se positionner pour jouer la première note à l'instant 0
    t0=time.perf_counter()*1000+2*1000
    t=t0   
    while current_note<len(l):        
        t=time.perf_counter()*1000-t0
        #si le temps écoulé depuis le début est superieur à l'instant auquel doit être joué la prochaine note moins le temps d'action
        #et que l'on a pas demandé de jouer la note, alors on donne l'ordre de jouer la note
        if l[current_note][0]<t and not note_en_cours:
            print("début")
            note_en_cours=True
            writer.write((l[current_note][2]+"@START"+"\r\n").encode())
            await writer.drain()
        elif l[current_note][1]<t and note_en_cours :
            print('fin')
            note_en_cours=False
            writer.write((l[current_note][2]+"@STOP"+"\r\n").encode())
            await writer.drain()
            current_note+=1
        await asyncio.sleep(1/1000)
    writer.close()
    await writer.wait_closed()       
    

"""
Cette fonction traite les connexions au serveur qui produit les notes en lisant le fichier MIDI
"""
async def handle_midi(filename, midiwriter) :
  # La tâche est créée pour exécuter readMIDI en lisant le fichier toto.mid
  # et en envoyant le résultat sur le flux midiwriter
  asyncio.create_task(readMIDI(filename, midiwriter))

