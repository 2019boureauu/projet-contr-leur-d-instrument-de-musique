import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import javax.swing.JTextArea;
import com.fazecast.jSerialComm.SerialPort;
// On importe toutes les biblioth�ques utilis�es.
// La classe IO g�re la console pour afficher les logs en temps r�el
// et permettre un suivi du d�roulement du programme.
// Elle g�re aussi quelques erreurs.

public class IO{
	SerialPort pyboard=null;
	PrintStream output;
	BufferedReader input;
	// Quelques objets utilis�s dans la classe.
	
	public IO(JTextArea console) {
		// On donne en param�tre au constructeur la console utilis�e pour afficher le texte.
        SerialPort[] array = SerialPort.getCommPorts();
        // On cherche tous les p�riph�riques USB connect�s
        for(SerialPort port : array) {
        	System.out.println(port.getPortDescription());
        	if(port.getPortDescription().contains("Pyboard")) {
        		pyboard = port;
        		pyboard.openPort();
        	}
        }
        // On teste si chaque p�riph�rique est une pyboard. Si oui, on ouvre la communication.
        
        if(pyboard==null) {
        	// Si on n'a pas de pyboard, on ne fait rien.
        }
        else {
        	// Dans le cas contraire, on �tablit l'envoi et la r�ception de donn�e.
        	output = new PrintStream(pyboard.getOutputStream());
        	try {
				input = new BufferedReader(new InputStreamReader(pyboard.getInputStream(), "UTF-8"));
			} catch (UnsupportedEncodingException e) {
				// Affichage des erreurs �ventuelles
				e.printStackTrace();
				console.append(e.getMessage());
			}
        }
	}
	
	// Quelques m�thodes utiles
	
	public void write(String str){
		// M�thode pour envoyer les informations � la pyboard
		output.append(str);
	}
	
	public String read() throws IOException {
		// M�thode pour lire les informations que la pyboard envoie
		return input.readLine();
	}
	
	public boolean ask() {
		// Teste si la pyboard est ouverte ou non
    	try {
    		return pyboard.isOpen();
    	}
    	catch (Exception e) {
    		return false;
    	}
	}
}
