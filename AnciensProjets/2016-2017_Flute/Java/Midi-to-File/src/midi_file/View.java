package midi_file;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Classe g�rant la vue.
 * Contient tous les �l�ments relatifs � l'interface graphique du programme.
 */
public class View {
	
	private ReadMidi rm;
	private ToPyboard toPyb;
	
	private JFrame f;
	private JTextField textMidFile;
	private JTextField BPMText;
	private JTextField targetText;
	private JLabel notesLabel;
	private JLabel confirmLabel;
	private JLabel confirmLabelTxt;

	/**
	 * Constructeur de la classe.
	 * G�n�re la premi�re fen�tre de la vue.
	 * 
	 * @param rmIn instance de la classe ReadMidi
	 * @param toPybIn instance de la classe ToPyboard
	 */
	public View(ReadMidi rmIn, ToPyboard toPybIn) {
		// ReadMidi et ToPyboard
		this.rm = rmIn;
		this.toPyb = toPybIn;
		
		// Initialisation Fen�tre
		this.f = new JFrame("Midi to Python");
		this.f.setVisible(true);
		this.f.setSize(400, 200);
		this.f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel p = new JPanel();
		p.setLayout(new GridBagLayout());
		GridBagConstraints c;
		this.f.add(p);
		
		double weightY = 0.5;
		
		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.weighty = weightY;
		c.gridwidth = 2;
		p.add(new JLabel("Fichier Midi"), c);
		
		
		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 1;
		c.weightx = 0.1;
		c.weighty = weightY;
		c.anchor = GridBagConstraints.LINE_START;
		p.add(new JLabel("<html>Entrez le r�pertoire<br>du fichier midi :</html>"), c);
		
		c = new GridBagConstraints();
		c.gridx = 1;
		c.gridy = 1;
		c.weightx = 0.9;
		c.weighty = weightY;
		c.fill = GridBagConstraints.BOTH;
		this.textMidFile = new JTextField("fichiersMidi\\flute_au_clair_de_la_lune.mid");
		p.add(this.textMidFile, c);

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 2;
		c.weighty = weightY;
		c.gridwidth = 2;
		JButton submit = new JButton("Valider");
		submit.addActionListener(new ButtonListener(this));
		p.add(submit, c);
	}
	
	/**
	 * G�n�re la deuxi�me fen�tre (une fois le chemin du fichier midi valid�).
	 * 
	 * @param path chemin du fichier midi
	 */
	public void updateView2(String path) {
		boolean proceed = true;
		// On initialise la lecture du fichier midi
		try {
			this.rm.initReadMidi(path);
			
		} catch (Exception e1) { // On affiche l'erreur sur l'interface graphique
			this.error("Le chemin du fichier midi n'est pas valide !");
			proceed = false;
			e1.printStackTrace();
		}
		
		// En absence d'erreur
		if(proceed) {
			// On cr�e une nouvelle vue
			this.f = new JFrame("Midi to Python");
			this.f.setVisible(true);
			this.f.setSize(600, 600);
			this.f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			
			// Panel et �l�ments de mise en page
			JPanel p = new JPanel();
			p.setLayout(new GridBagLayout());
			GridBagConstraints c;
			this.f.add(p);
			
			double weightX = 0.05;
			double weightY = 0.2;
			
			
			// Titre
			c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = 0;
			c.weightx = weightX;
			c.weighty = weightY;
			c.gridwidth = 3;
			JPanel title = new JPanel();
			title.add(new JLabel("Entrez le BPM du fichier midi puis proc�dez � la cr�ation d'un fichier Python"));
			p.add(title, c);
			
			
			// Gestion BPM
			c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = 1;
			c.weightx = weightX;
			c.weighty = weightY;
			c.anchor = GridBagConstraints.CENTER;
			c.fill = GridBagConstraints.BOTH;
			p.add(new JLabel("Entrez le BPM : "), c);
			
			c = new GridBagConstraints();
			c.gridx = 1;
			c.gridy = 1;
			c.weightx = 0.9;
			c.weighty = weightY;
			c.anchor = GridBagConstraints.CENTER;
			c.fill = GridBagConstraints.BOTH;
			this.BPMText = new JTextField("120");
			p.add(this.BPMText, c);
			
			c = new GridBagConstraints();
			c.gridx = 2;
			c.gridy = 1;
			c.weightx = weightX;
			c.weighty = weightY;
			c.anchor = GridBagConstraints.CENTER;
			c.fill = GridBagConstraints.BOTH;
			JButton submitBPM = new JButton("Set BPM");
			submitBPM.addActionListener(new ButtonListener(this));
			p.add(submitBPM, c);
			
			
			// Gestion du dossier cible (pour le fichier Python)
			c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = 2;
			c.weightx = weightX;
			c.weighty = weightY;
			c.anchor = GridBagConstraints.CENTER;
			c.fill = GridBagConstraints.BOTH;
			p.add(new JLabel("<html>Entrez le r�pertoire<br>cible du fichier Python :</html>"), c);
			
			c = new GridBagConstraints();
			c.gridx = 1;
			c.gridy = 2;
			c.weightx = 0.9;
			c.weighty = weightY;
			c.anchor = GridBagConstraints.CENTER;
			c.fill = GridBagConstraints.BOTH;
			this.targetText = new JTextField("renduNotes\\");		
			p.add(this.targetText, c);
			
			c = new GridBagConstraints();
			c.gridx = 2;
			c.gridy = 2;
			c.weightx = weightX;
			c.weighty = weightY;
			c.anchor = GridBagConstraints.CENTER;
			c.fill = GridBagConstraints.BOTH;
			JButton submitTarget = new JButton("Set Target Repertory");
			submitTarget.addActionListener(new ButtonListener(this));
			p.add(submitTarget, c);
			
			// Affichage des infos et de la liste de notes
			c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = 3;
			c.weightx = weightX;
			c.weighty = 0.5;
			c.anchor = GridBagConstraints.FIRST_LINE_START;
			p.add(new JLabel("Informations fichier Midi : "), c);
			
			c = new GridBagConstraints();
			c.gridx = 1;
			c.gridy = 3;
			c.weightx = 0.9;
			c.weighty = 0.5;
			c.anchor = GridBagConstraints.FIRST_LINE_START;
			c.gridwidth = 2;
			int l = rm.getNumberTracks();
			String infoMidi = "Number of tracks : " + l + "<br>";
			for(int i=0 ; i < l ; i++){
				infoMidi += "Last index of track num " + i + " : " + this.rm.getTrackEndIndex().get(i) + "<br>";
			}			
			p.add(new JLabel("<html>" + infoMidi + "</html>"), c);
			
			c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = 4;
			c.weightx = weightX;
			c.weighty = 0.9;
			c.anchor = GridBagConstraints.FIRST_LINE_START;
			p.add(new JLabel("Liste des notes : "), c);
			
			c = new GridBagConstraints();
			c.gridx = 1;
			c.gridy = 4;
			c.weightx = 0.9;
			c.weighty = 0.9;
			c.anchor = GridBagConstraints.FIRST_LINE_START;
			c.gridwidth = 2;
			this.notesLabel = new JLabel("DEFINIR LE BPM");
			p.add(this.notesLabel, c);
			
			// Bouton de creation de fichier Python
			c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = 5;
			c.weightx = weightX;
			c.weighty = weightY;
			c.gridwidth = 3;
			JButton submitSave = new JButton("Create Python File");
			submitSave.addActionListener(new ButtonListener(this));
			p.add(submitSave, c);
			
			c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = 6;
			c.weighty = 0.001;
			c.gridwidth = 3;
			this.confirmLabel = new JLabel();
			p.add(this.confirmLabel, c);
			
			// Bouton de creation de fichier Python
			c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = 7;
			c.weightx = weightX;
			c.weighty = weightY;
			c.gridwidth = 3;
			JButton submitSaveTxt = new JButton("Create Txt File");
			submitSaveTxt.addActionListener(new ButtonListener(this));
			p.add(submitSaveTxt, c);
						
			c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = 8;
			c.weighty = 0.001;
			c.gridwidth = 3;
			this.confirmLabelTxt = new JLabel();
			p.add(this.confirmLabelTxt, c);
		}
	}

	/**
	 * Getter retournant l'input du chemin du fichier midi.
	 * 
	 * @return textMidFile input du chemin du fichier midi
	 */
	public JTextField getTextMidFile() {
		return(this.textMidFile);
	}
	
	/**
	 * Getter retournant l'input du BPM choisi pour le fichier midi.
	 * @return BPMText input du BPM
	 */
	public JTextField getBPMText() {
		return(this.BPMText);
	}
	
	/**
	 * Getter retournant l'input du chemin du dossier de sauvegarde.
	 * 
	 * @return targetText input du chemin du dossier de sauvegarde
	 */
	public JTextField getTargetText() {
		return(this.targetText);
	}
	
	/**
	 * Setter permettant de fixer le BPM choisi pour le fichier midi.
	 * 
	 * @param BPMIn BPM � fixer
	 */
	public void setBPM(int BPMIn) {
		this.BPMText.setText("OK");
		this.toPyb.setBPM(BPMIn);
		// Une fois le BPM d�fini, on peut initialiser le tableau toPyb.notes
		this.toPyb.initNotes(this.rm);
		this.notesLabel.setText("<html><div style=\"width:300px\">" + this.toPyb.notesToString() + "</div></html>"); // Ce code html encadre notre texte au sein d'une balise div de largeur fix�e pour permettre un retrour � la ligne en cas de d�passement.
	}
	
	/**
	 * Setter permettant de fixer le chemin du dossier de sauvegarde.
	 * 
	 * @param targetIn chemin � fixer
	 */
	public void setTarget(String targetIn) {
		boolean proceed = true;
		
		try {
			this.toPyb.setTarget(targetIn);	
		} 
		catch (Exception e1) { // On affiche l'erreur sur l'interface graphique
			this.error("Le chemin sp�cifi� n'est pas valide !<br>Entrez un chemin vide ou un chemin terminant par '\'");
			proceed = false;
			e1.printStackTrace();
		}
		
		if(proceed) {
			this.targetText.setText("OK");
		}
	}
	
	
	/**
	 * Demande � l'instance de ToPyboard de cr�er un fichier .py.
	 */
	public void createPyFile() {
		this.toPyb.toPyFile();
		this.confirmLabel.setText("Fichier python cr��");
	}
	
	/**
	 * Demande � l'instance de ToPyboard de cr�er un fichier .txt.
	 */
	public void createTxtFile() {
		this.toPyb.toTxtFile();
		this.confirmLabelTxt.setText("Fichier texte cr��");
	}
	
	
	/**
	 * M�thode affichant les messages d'erreur.
	 * 
	 * @param info information sur l'erreur � afficher
	 */
	public void error(String info) {
		JFrame fError = new JFrame("Midi to Python");
		fError.setVisible(true);
		fError.setSize(400, 100);
		
		JPanel p = new JPanel();
		p.setLayout(new GridBagLayout());
		fError.add(p);
		
		p.add(new JLabel("<html>Erreur : " + info + "</html>"));
	}
	
}
