from tkinter import * #Importation du module tkinter
#Définition de la variable principale
m=0 #prendre la prise d'origine du système
#définition des fonctions
def Do():
    m=1
    num1.set("Do")
    return 

def Dod():
    m=2
    num1.set("Do#")
    return 

def Re():
    m=3
    num1.set("Ré")
    return 

def Mib():
    m=4
    num1.set("Mib")
    return

def Mi():
    m=5
    num1.set("Mi")
    return 

def Fa():
    m=6
    num1.set("Fa")
    return 

def Fad():
    m=7
    num1.set("Fad")
    return

def Sol():
    m=8
    num1.set("Sol")
    return

def Sold():
    m=9
    num1.set("Sold")
    return

def La():
    m=10
    num1.set("La")
    return 

def Si():
    m=11
    num1.set("Si")
    return 

def Sib():
    m=12
    num1.set("Sib")
    return 

root=Tk()
frame=Frame(root)
frame.pack()
root.title("Clavier MIDI")
num1=StringVar()

topframe=Frame(root)
topframe.pack(side=TOP)
txtDisplay=Entry(frame, textvariable=num1,bd=20, insertwidth=1, font=30, justify="center", width=4,)
txtDisplay.pack(side=TOP)

touche1=Button(topframe,padx=8, height=6,pady=8,bd=8, text="Do#" ,bg="black",fg="white", command=Dod)
touche1.pack(side=LEFT)
touche22=Button(topframe, state=DISABLED, height=7, width=1, padx=0, pady=0, relief=RIDGE)
touche22.pack(side=LEFT)
touche2=Button(topframe,padx=8, height=6,pady=8,bd=8, text="Mib" ,bg="black",fg="white", command=Mib)
touche2.pack(side=LEFT)
touche22=Button(topframe, state=DISABLED, height=7, width=1, padx=0, pady=0, relief=RIDGE)
touche22.pack(side=LEFT)
touche3=Button(topframe,padx=8, height=6,pady=8,bd=8, text="Fa#" ,bg="black",fg="white", command=Fad)
touche3.pack(side=LEFT)
touche22=Button(topframe, state=DISABLED, height=7, width=1, padx=0, pady=0, relief=RIDGE)
touche22.pack(side=LEFT)
touche4=Button(topframe,padx=8, height=6,pady=8,bd=8, text="Sol#" ,bg="black",fg="white", command=Sold)
touche4.pack(side=LEFT)
touche22=Button(topframe, state=DISABLED, height=7, width=1, padx=0, pady=0, relief=RIDGE)
touche22.pack(side=LEFT)
touche2=Button(topframe,padx=8, height=6,pady=8,bd=8, text="Do#" ,bg="black",fg="white", command=Dod)
touche2.pack(side=LEFT)
touche22=Button(topframe, state=DISABLED, height=7, width=1, padx=0, pady=0, relief=RIDGE)
touche22.pack(side=LEFT)


#Lancer la boucle 
mainloop()


