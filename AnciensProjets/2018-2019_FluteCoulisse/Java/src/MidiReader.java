import java.awt.Dimension;
import java.io.File;
import java.io.IOException;

import javax.sound.midi.*;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

public class MidiReader {
	// Ces variables permettent d'initialiser les notes MIDI
    public static final int NOTE_ON = 0x90;
    public static final int NOTE_OFF = 0x80;
    public static final String[] NOTE_NAMES = {"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"};
    // On g�n�re aussi une console d'affichage des logs ainsi qu'un objet IO
    private static JFrame frame;
    private static JTextArea consoleText = new JTextArea();
    private static IO io = new IO(consoleText);
    
    
    // Au lancement du programme, on teste toutes les erreurs possibles
    // La m�thode cout permet d'�crire dans la fen�tre "console" les logs de la communication
    public static void main(String[] args) throws Exception {
    	
    	// On demande � l'utilisateur de donner l'emplacement du fichier MIDI
    	String directory = getDirectory();
    	
    	// On �crit dans la console d'Eclipse uniquement la destination indiqu�e
    	// Cela ne sert qu'au d�boggage
    	System.out.println(directory);
    	if(directory.equals("errorNoFileChosen")) {
    		// Si on n'a pas donn� de fichier
    		cout("Erreur : veuillez s�lectionner un fichier\r\n");
    	}
    	else if(!isMono(directory)) {
    		// Si le fichier MIDI choisi n'est pas monodique (une note � la fois)
    		cout("Erreur : le fichier s�lectionn� n'est pas mono\r\n");
    	}
    	else if(directory.equals("errorNoPyboard")) {
    		// Si on n'a pas branch� de pyboard
    		cout("Erreur : pas de pyboard d�tect�e\r\n");
    	}
    	else {
    		// Dans tous les autres cas, on peut envoyer les informations � la pyboard !
    		cout("\nLecture du fichier midi\r\n\n");
    		midiSend(directory);
    	}
    }
    
    
    // Quelques m�thodes utiles
    
    private static String getDirectory() {
    	// Cette m�thode permet d'obtenir le fichier midi � lire via une interface utilisateur
    	frame = new JFrame("MidiReader");
    	// Fermer le programme � la fermeture de la fen�tre
    	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	// On donne les dimensions
    	frame.setPreferredSize(new Dimension(720, 480));
    	// On y ajoute la console de logs et on compile la "frame"
    	frame.getContentPane().add(consoleText);
    	frame.pack();
    	frame.setVisible(true);
    	char sep = File.separatorChar;
    	if(io.ask()==true) {
    		// On a d�tect� la pyboard
    		JFileChooser chooser = new JFileChooser(System.getProperty("user.home") + sep + "Desktop");
    		FileNameExtensionFilter filter = new FileNameExtensionFilter("Fichiers MIDI", "mid");
    		// Par d�faut, on indique de chercher les fichiers MIDI sur le bureau
    		chooser.setFileFilter(filter);
    		int returnVal = chooser.showOpenDialog(frame);
    		if(returnVal == JFileChooser.APPROVE_OPTION) {
    			// Si on peut bien ouvrir le fichier, il est s�lectionn�
    			System.out.println("You chose to open this file: " +
    					chooser.getSelectedFile().getName());
    			return chooser.getSelectedFile().getPath();
    		}
    		else {
    			// Dans le as contraire, on renvoie une erreur
    			return "errorNoFileChosen";
    		}
    		
    	}
    	// Cette ligne n'est accessible que si aucune pyboard n'est connect�e
        return "errorNoPyboard";
    }
    
    static void cout(String str) {
    	// On �crit dans la console
    	consoleText.append(str);
    }
    
    static void midiSend(String directory) throws InvalidMidiDataException, IOException, InterruptedException {
    	// Cette m�thode envoie les informations MIDI en temps r�el � la pyboard
    	int bpm = 0 ;
    	float msUnit = 0;
    	// On charge le fichier midi choisi
        Sequence sequence = MidiSystem.getSequence(new File(directory));
        // On d�termine le tempo du fichier (ppq)
        float ppq = sequence.getResolution();
        long tickTracking = 0;
        String lastNotePlayed = "";
        
    	for (Track track :  sequence.getTracks()) {
            for (int i=0; i < track.size(); i++) { 
            	// On lit tous les �v�nements des pistes MIDI
                MidiEvent event = track.get(i);
                MidiMessage message = event.getMessage();
                if (message instanceof ShortMessage) {
                	// On a un d�but ou un arr�t de note
                    ShortMessage sm = (ShortMessage) message;
                    if (sm.getCommand() == NOTE_ON) {
                    	// On attend avant de jouer la prochaine note
                    	Thread.sleep((long) msUnit*(event.getTick()-tickTracking));
                    	tickTracking = event.getTick();
                    	// On d�termine la note jou�e, son octave et son alt�ration
                    	int key = sm.getData1();
                        int octave = (key / 12)-1;
                        int note = key % 12;
                        String noteName = NOTE_NAMES[note];
                        lastNotePlayed = noteName;
                        int velocity = sm.getData2();
                        // Communique avec la pyboard et affiche les infos relative � la note en cours
                        // Pour jouer une note, on envoie le message format� par "NOTE@VELOCITY" s�par� par un espace (cf. protocole)
                        io.write(noteName + octave + "@" + velocity +"\r\n");
                        // On l'affiche �galement dans la console Eclipse et la console de logs
                        System.out.println("Note on, " + noteName + octave + " key=" + key + " velocity: " + velocity);
                        cout("Note on, " + noteName + octave + " key=" + key + " velocity: " + velocity +"\r\n");
                    } else if (sm.getCommand() == NOTE_OFF) {
                    	Thread.sleep((long) msUnit*(event.getTick()-tickTracking));
                    	// Idem, on attend avant d'arr�ter la note
                    	tickTracking = event.getTick();
                    	int key = sm.getData1();
                    	int octave = (key / 12)-1;
                    	int note = key % 12;
                    	String noteName = NOTE_NAMES[note];
                    	if(noteName.contentEquals(lastNotePlayed));
                    		// Pour arr�ter une note en cours, on envoie le message "NOTE@STOP"
                    		io.write(noteName + octave + "@STOP\r\n");
                    		System.out.println(noteName + octave + "  stop");	
                    }
                } else {
                    // Gestion du tempo du morceau
                    if (message instanceof MetaMessage) {
                        MetaMessage mm = (MetaMessage) message;
                        int SET_TEMPO = 81;			// Numero de MetaMessage pour le SET_TEMPO
                        if(mm.getType()==SET_TEMPO){
                        	byte[] data = mm.getData();
                        	int tempo = (data[0] & 0xff) << 16 | (data[1] & 0xff) << 8 | (data[2] & 0xff);
                        	bpm = 60000000 / tempo;
                        	msUnit = 60000/(bpm*ppq);
                        }
                    }
                }
            }
        }
    }
    
    
    static boolean isMono(String directory) throws InvalidMidiDataException, IOException {
    	// De m�me, on charge la s�quence MIDI
        Sequence sequence = MidiSystem.getSequence(new File(directory));
        long lastTickPlayed = -1;
        
    	for (Track track :  sequence.getTracks()) {
            // On r�cup�re les pistes jou�es
            for (int i=0; i < track.size(); i++) { 
                MidiEvent event = track.get(i);
                MidiMessage message = event.getMessage();
                if (message instanceof ShortMessage) {
                    ShortMessage sm = (ShortMessage) message;
                    if (sm.getCommand() == NOTE_ON ) {
                    	if(event.getTick()==lastTickPlayed) {
                    		// Si la prochaine note jou�e est d�clench�e alors qu'une autre note
                    		// joue, on termine la m�thode et on renvoie que le fichier n'est pas monophonique
                    		return false;
                    	}
                    	else {
                    		// Sinon, on continue de se d�placer dans la piste
                    		lastTickPlayed = event.getTick();
                    	}
                    }
                }
            }
        }
    	// Si on n'a rencontr� aucune erreur, le fichier est bien monophonique.
    	return true;
    }
}







