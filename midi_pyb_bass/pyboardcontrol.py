import asyncio

####
# On utilise asyncio pour avoir plusieurs tâches qui s'exécutent en même temps, 
# mais de manière coopérative. Aini, une tâche ne cède du temps aux autres que 
# volontairement, quand elle attend autre chose, grâce à "await"
# Ces tâches sont déclarées "async" de façon à pouvoir être gérées par un ordonnanceur 
# de tâche qui est mis en œuvre par asyncio.run().
#
# La communication entre les tâches se fait en mode client-serveur. On crée un
# serveur pour chaque tâche, et ceux qui veulent utiliser le résultat de la tâche
# se connectent au serveur.
###

"""
Cette fonction lit des données ligne par ligne sur un flux d'entrée,
les interprète comme des nom de note anglosaxons, et écrit les notes
à l'italienne sur son flux de sortie.
L'idée est de simuler le comportement de la pyboard qui reçoit les noms de notes
et envoie des commandes aux actionneurs pour jouer la note sur la guitare.
"""
async def pyboard(reader, writer) :
  # On lit une ligne sur le flux d'entrée
  l = await reader.readline()
  # Tant qu'on lit quelque chose
  while not ((l is None) or (len(l) == 0)) :
    # On décode les données brutes (bytes) pour obtenir une chaîne de caractères
    # et on supprime les blancs en tête et en fin de chaîne
    l = l.decode().strip()
    # On reconnaît la note et on écrit son nom sur le flux de sortie.
    # On ajoute "\r\n" pour avoir une fin de ligne quelle que soit la plateforme
    # (Mac, Windows, Linux), et on utilise encode() pour transformer la chaîne
    # de caractères en bytes (c'est ce qu'attend la méthode write() du flux)
    if l == "A4" :
      writer.write("La\r\n".encode())
      await writer.drain()
    elif l == "B4" :
      writer.write("Si\r\n".encode())
      await writer.drain()
    elif l == "C5" :
      writer.write("Do\r\n".encode())
      await writer.drain()
    elif l == "D5" :
      writer.write("Ré\r\n".encode())
      await writer.drain()
    elif l == "E5" :
      writer.write("Mi\r\n".encode())
      await writer.drain()
    elif l == "F5" :
      writer.write("Fa\r\n".encode())
      await writer.drain()
    elif l == "G5" :
      writer.write("Sol\r\n".encode())
      await writer.drain()
    else :
      print("# Error, unknown note: " + l)
    # On lit la ligne suivante et on reboucle
    l = await reader.readline()
  # Quand c'est fini, on ferme le flux de sortie pour indiquer au suivant qu'il n'y a plus rien à lire.
  writer.close()
  await writer.wait_closed()

"""
Cette fonction traite les connexions au serveur qui traduit les notes MIDI
"""
async def handle_pyboard(pybreader, pybwriter) :
  asyncio.create_task(pyboard(pybreader, pybwriter))


if __name__ == "__main__":
  """
  Ce programme de test crée un serveur sur la machine (127.0.0.1 est l'adresse de la 
  machine locale) qui écoute les requêtes de connexion sur le port 10000.
  Quand une connexion est demandée, la fonction handle_pyboard est appelée.

  Le programme se connecte ensuite à ce serveur, lit chacune des lignes produites
  par le serveur et les affiche dans la console.
  """
  async def main() :
    # Lancement du serveur sur le port 10000
    await asyncio.start_server(handle_pyboard, host="127.0.0.1", port=10000)
    # Connexion au serveur, ce qui rend une paire de flux permettant 
    # de recevoir et d'envoyer des données au serveur
    (pybreader, pybwriter) = await asyncio.open_connection(host="127.0.0.1", port=10000)
    # Données utilisées pour tester le programme
    input = ("A4", "B4", "C5", "D5", "E5", "F5", "G5")
    # Résultats obtenus
    result = []
    # Pour chaque donnée à tester
    for n in input :
      # On envoie la donnée au serveur (avec fin de ligne pour que readline marche bien)
      pybwriter.write((n + "\r\n").encode())
      # On attend que les données soient parties
      await pybwriter.drain()
      # On lit la réponse du serveur
      l = await pybreader.readline()
      # Et on l'ajoute (décodée et strippée) au tableau des résultats
      result.append(l.decode().strip())
    # Quand c'est fini, on affiche sur la console les données envoyées et les résultats obtenus.
    print(str(input) + "\r\n--> " + str(result))

  # On exécute le programme de test comme une tâche asyncio
  asyncio.run(main())
