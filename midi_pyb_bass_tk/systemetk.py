import asyncio
import tkinter as tk

####
# On utilise asyncio pour avoir plusieurs tâches qui s'exécutent en même temps, 
# mais de manière coopérative. Aini, une tâche ne cède du temps aux autres que 
# volontairement, quand elle attend autre chose, grâce à "await"
# Ces tâches sont déclarées "async" de façon à pouvoir être gérées par un ordonnanceur 
# de tâche qui est mis en œuvre par asyncio.run().
#
# La communication entre les tâches se fait en mode client-serveur. On crée un
# serveur pour chaque tâche, et ceux qui veulent utiliser le résultat de la tâche
# se connectent au serveur.
###

####
# On importe les tâches de lecture MIDI, traitement pyboard et interface de la basse
####
from readmidi import readMIDI, handle_midi
from pyboardcontrol import pyboard, handle_pyboard
from tkinterbass import bassguitar, handle_guitar, createBassWindow

"""
Ce programme principal crée les serveurs associés aux 3 tâches et les interconnecte
via leurs flux d'entrée et de sortie. Il lit le résultat sur le flux de sortie de la
tâche qui simle la guitare basse et l'affiche sur la console.
"""
async def main() :
  # Création du serveur MIDI sur le port 10000
  await asyncio.start_server(handle_midi, host="127.0.0.1", port=10000)
  # Connexion au serveur MIDI. Le flux midiwriter ne sert à rien puisque ce serveur ne 
  # fait que nous envoyer le contenu du fichier ligne par ligne sur le flux midireader
  (midireader, midiwriter) = await asyncio.open_connection(host="127.0.0.1", port=10000)
  
  # Création du serveur pyboard sur le port 10001.
  # Il est important de noter que la fonction qui traite les connexions donne midireader
  # comme flux d'entrée à la tâche, de façon à ce qu'elle lise ce que la tâche MIDI produit.
  await asyncio.start_server(lambda r, w: handle_pyboard(midireader, w), host="127.0.0.1", port=10001)
  # On se connecte au serveur pyboard, et pybreader permet de lire ce produit la pyboard
  # tandis que pybwriter permet de lui envoyer des données (mais on ne s'en sert pas car 
  # c'est la tâche MIDI qui envoie les données à la pyboard).
  (pybreader, pybwriter) = await asyncio.open_connection(host="127.0.0.1", port=10001)
  #
  # On initialise tkinter et on créer l'interface graphique pour la basse
  tk_main = tk.Tk()
  tk_main.withdraw()  # cacher la fenêtre racine
  createBassWindow(tk_main, asyncio.get_event_loop())

  # Enfin, on crée le serveur pour la guitare, et la fonction qui traite les connexions
  # lui donne pybreader comme flux d'entrée afin que ce serveur lise ce que produit
  # la pyboard.
  await asyncio.start_server(lambda r, w: handle_guitar(pybreader, w), host="127.0.0.1", port=10002)
  # On se connecte à ce serveur, mais on n'utilise pas ses flux car il affiche avec Tkinter.
  (bassreader, basswriter) = await asyncio.open_connection(host="127.0.0.1", port=10002)
  
  # Wait until all tasks (but this one and the update task of the bass guitar) are finished
  while len(asyncio.all_tasks()) > 2 :
    await asyncio.sleep(1)


# Exécute la fonction main comme une tâche asyncio
asyncio.run(main())

#
# La tâche MIDI lit le fichier MIDI et écrit son contenu sur le flux A, qui
# est le writer du serveur MIDI et le reader du serveur pyboard.
# La tâche pyboard lit sur le flux A ce que produit la tâche MIDI et écrit ses résultats
# sur le flux B, qui est le writer du serveur pyboard et le reader du serveur guitare.
# La tâche guitare lit sur le flux B ce que produit la tâche pyboard, et écrit ses
# résultats sur le flux C, qui est le writer du serveur guitare.
# Enfin, la boucle dans le programme main lit sur le flux C ce que produit la tâche 
# guitare et l'écrit sur la console.
#
# Il serait possible d'avoir des boucles d'interaction entre les tâches, il suffirait 
# pour cela de leur passer en argument le reader et le writer d'un même serveur afin
# qu'elle puisse lui envoyer des données autant qu'en recevoir.
#
# +---------+       +---------+       +---------+       +---------+
# |         |       |         |       |         |       |         |
# |         |       |         |       |         |       |         |
# | Fichier |       | Tâche   |   A   | Tâche   |   B   | Tâche   |
# |         | ----> |         | ----> |         | ----> |         | ----> Fenêtre Tkinter
# | MIDI    |       | MIDI    |       | pyboard |       | guitare |
# |         |       |         |       |         |       |         |
# |         |       |         |       |         |       |         |
# +---------+       +---------+       +---------+       +---------+
#